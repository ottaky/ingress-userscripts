// ==UserScript==
// @name           ottaky ingress augment
// @version        0.32
// @description    ottaky ingress augment
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function inject(func) {
  
  var source = func.toString();
  var script = document.createElement('script');
  script.innerHTML = "(" + source + ")()";
  document.body.appendChild(script);
}

function myscript() {
    
  if (typeof window.nemesis.dashboard.soy.portalinfooverlay.main == "function" &&
      typeof window.nemesis.dashboard.render.PortalMarker.getGlowIcon_ == "function") {

    window.nemesis.ottaky_links = {}; // links count
      
    // this adds a local (session only) cache for link data
      
    window.nemesis.dashboard.data.Edge.create = function(a, b, c) {
          
      a = new nemesis.dashboard.data.Edge(a, b.team, c);
      a.origin_ = new nemesis.dashboard.data.Point(b.oGuid, b.oLatE6 / 1E6, b.oLngE6 / 1E6);
      a.dest_ = new nemesis.dashboard.data.Point(b.dGuid, b.dLatE6 / 1E6, b.dLngE6 / 1E6);
          
      // links cache
          
      window.nemesis.ottaky_links[a.guid] = {'origin': { 'lat': a.origin_.lat, 'lng': a.origin_.lng },
                                             'destination': {'lat': a.dest_.lat, 'lng': a.dest_.lng } };

      a.getNewPoints_();
      return a;
    };
     
    // if there's an ll as the 1st parameter in the URL (used to centre the map somewhere), draw a circle at that position
    // we have to delay execution of this so that the map has a chance to initialise
    // ugly
      
    var sjb_circle = function() {

      if (document.location.search.substr(1).split('&')[0].split('=')[0] == 'll') {

        var sjb_ll = document.location.search.substr(1).split('&')[0].split('=')[1].split(',');

        var sjb_c = new google.maps.Circle({'strokeColor': '#FFFFFF',
                                            'strokeOpacity': 0.8,
                                            'strokeWeight': 2,
                                            'map': nemesis.dashboard.maputils.map,
                                            'center': new google.maps.LatLng(parseFloat(sjb_ll[0]),
                                                                             parseFloat(sjb_ll[1])),
                                            'radius': 50});
      }
    };
  
    setTimeout(sjb_circle, 1000);
      
    // instead of clicking on the resonators and mods tabs and close button, mouseover
      
    window.nemesis.dashboard.render.PortalInfoOverlay.prototype.renderPortalDetails_ = function() {
      soy.renderElement(this.div_, nemesis.dashboard.soy.portalinfooverlay.main, {portal:this.portal_, artifactFragmentNumbers:this.portal_.getArtifactFragmentNums().join(", ")});
      var a = document.getElementById("pi-tab-res");
      a && this.eventHandler_.listen(a, goog.events.EventType.MOUSEOVER, goog.bind(this.onSelectTab_, this, "resonators"));
      (a = document.getElementById("pi-tab-mod")) && this.eventHandler_.listen(a, goog.events.EventType.MOUSEOVER, goog.bind(this.onSelectTab_, this, "mods"));
      a = document.getElementById("portal_close");
      this.eventHandler_.listen(a, goog.events.EventType.MOUSEOVER, this.onClose_);
      this.mapsListener_ = google.maps.event.addListener(nemesis.dashboard.maputils.map, "click", goog.bind(function(a) {
        if (this.shouldHandleEvent_("tab")) {
          this.onClose_(a);
        } else {
          nemesis.dashboard.render.PortalInfoOverlay.currentInfoWindow_.setInfoWindowPosition_();
        }
      }, this));
    };
      
    // pre-fill the search box because I get tired of typing it in
    
    document.getElementById('address').value = 'shooters hill';  

    // de-clutter the page when browser is full screen / in kiosk mode
    // see http://ottaky.com/ingress/ingress_map_annotated.png
      
    if (window.outerWidth > screen.width - 10) {
          
      document.getElementById("comm").style.visibility = 'hidden';
      document.getElementById("player_stats").style.visibility = 'hidden';
      document.getElementById("header_login_info").style.visibility = 'hidden';
      document.getElementById("filters_container").style.visibility = 'hidden';
    }

	// set the annoying "human presence not detected .." popup timeout to 20 minutes instead of 2 (I hate that bloody popup)
      
    window.nemesis.dashboard.Dashboard.REFRESH_DATA_WAIT_MS_ = 12E5;

    // use 24 hour time format and add the time to old plexts
      
    window.nemesis.dashboard.data.Plext.timeFormatter_ = new goog.i18n.DateTimeFormat("HH:mm");
    window.nemesis.dashboard.data.Plext.dateFormatter_ = new goog.i18n.DateTimeFormat("dd HH:mm");
      
	// add incoming and outgoing links
      
    window.nemesis.dashboard.data.Portal.prototype.updateDetails = function(a) {
      this.hasDetails_ = !0;
      nemesis.dashboard.data.Portal.readSimplePortalData_(this, a);
      this.linkedResonators = nemesis.dashboard.data.Resonator.create(a.resonators);
      this.linkedMods = goog.array.map(a.mods, function(a) {
        return a ? {rarity:a.rarity, name:a.name, stats:a.stats, installer:a.owner} : null;
      });
      a.owner && (this.isCaptured = !0, this.capturingPlayer = a.owner);
          
      // links count (hokey)
        
      var outgoing_links = 0;
      var incoming_links = 0;
        
      for (var edge_guid in window.nemesis.ottaky_links) {
            
        if (window.nemesis.ottaky_links[edge_guid].origin.lat == (a.latE6 / 1E6) &&
            window.nemesis.ottaky_links[edge_guid].origin.lng == (a.lngE6 / 1E6)) {
             
          outgoing_links++;
        }
        else if (window.nemesis.ottaky_links[edge_guid].destination.lat == (a.latE6 / 1E6) &&
                 window.nemesis.ottaky_links[edge_guid].destination.lng == (a.lngE6 / 1E6)) {
             
          incoming_links++;
        }
      }
        

      this.outgoing_links = outgoing_links;
      this.incoming_links = incoming_links;
    };

      
    // show link data
      
    window.nemesis.dashboard.soy.portalinfooverlay.captureDetails = function(a, b) {
      var c = b || new soy.StringBuilder;
      c.append(a.portal.isCaptured ? a.portal.capturingPlayer ? '<span title="' + soy.$$escapeHtmlAttribute(a.portal.capturingPlayer) + '">' + soy.$$escapeHtml(a.portal.capturingPlayer) + "</span>" : "<span>loading&hellip;</span>" : "");
      c.append('<br>Links in ' + a.portal.incoming_links + ', links out ' + a.portal.outgoing_links + ' [' + (a.portal.incoming_links + a.portal.outgoing_links) + ']');
      return b ? "" : c.toString();
    };
      
    /* These 2 no longer works now that capturedTime has been removed (functions kept for historical reasons)
        
    window.nemesis.dashboard.data.Portal.prototype.updateDetails = function(a) {
      this.hasDetails_ = !0;
      nemesis.dashboard.data.Portal.readSimplePortalData_(this, a);
      this.linkedResonators = nemesis.dashboard.data.Resonator.create(a.resonators);
      this.linkedMods = goog.array.map(a.mods, function(a) {
          return a ? {rarity:a.rarity, name:a.name, stats:a.stats, installer:a.owner} : null;
      });

      a.capturedTime && (this.isCaptured = !0, this.capturedTime = (new goog.i18n.DateTimeFormat("dd/MM/yy HH:mm")).format(new Date(parseInt(a.capturedTime, 10))).replace(/\//g, "/&zwnj;") + ' [' + parseInt(((new Date().getTime()) - parseInt(a.capturedTime, 10)) / (1000*60*60*24)) + ']', this.capturingPlayer = a.owner);
    
      // links count
        
      var outgoing_links = 0;
      var incoming_links = 0;
        
      for (var edge_guid in window.nemesis.ottaky_links) {
            
        if (window.nemesis.ottaky_links[edge_guid].origin.lat == (a.latE6 / 1E6) &&
            window.nemesis.ottaky_links[edge_guid].origin.lng == (a.lngE6 / 1E6)) {
             
          outgoing_links++;
        }
        else if (window.nemesis.ottaky_links[edge_guid].destination.lat == (a.latE6 / 1E6) &&
                 window.nemesis.ottaky_links[edge_guid].destination.lng == (a.lngE6 / 1E6)) {
             
           incoming_links++;
        }
      }
        

      this.outgoing_links = outgoing_links;
      this.incoming_links = incoming_links;
    };

    window.nemesis.dashboard.soy.portalinfooverlay.captureDetails = function(a, b) {
      var c = b || new soy.StringBuilder;
      c.append(a.portal.isCaptured ? (a.portal.capturingPlayer ? '<span title="' + soy.$$escapeHtmlAttribute(a.portal.capturingPlayer) + '">' + soy.$$escapeHtml(a.portal.capturingPlayer) + "</span>" : "<span>loading&hellip;</span>") + (0 != a.portal.capturedTime ? "   " + a.portal.capturedTime : "") : "");
      c.append('<br>Links in ' + a.portal.incoming_links + ', links out ' + a.portal.outgoing_links + ' [' + (a.portal.incoming_links + a.portal.outgoing_links) + ']');
      return b ? "" : c.toString();
    };
*/

    // highlight friends in COMM (add your own)
      
    window.nemesis.ottaky_friends = { "ottaky":true, "diamondg":true, "neomirg":true, "richardiniho":true, "Weenopod":true };
      
    window.nemesis.dashboard.soy.plextrender.markup_ = function(a, b) {
      var c = b || new soy.StringBuilder;
      switch(a.type) {
        case nemesis.dashboard.data.PlextMarkup.Type.FACTION:
          c.append('<span class="', soy.$$escapeHtmlAttribute(a.team.css), '">', soy.$$escapeHtml(a.team.displayName), "</span>");
          break;
        case nemesis.dashboard.data.PlextMarkup.Type.PLAYER:
          c.append('<span class="', soy.$$escapeHtmlAttribute(a.team.css), ' pl_nudge_player" data-playerstr="', soy.$$escapeHtmlAttribute("@" + a.plain), '" data-isfaction="', soy.$$escapeHtmlAttribute(a.isFaction), '">', soy.$$escapeHtml(a.plain), "</span>");
          break;
        case nemesis.dashboard.data.PlextMarkup.Type.SENDER:
          if (window.nemesis.ottaky_friends[a.sender]) {
              c.append("<span ", PLAYER.nickname == a.sender ? 'class="' + soy.$$escapeHtmlAttribute(a.team.css) + '"' : 'class="' + soy.$$escapeHtmlAttribute(a.team.css) + ' pl_nudge_player" data-playerstr="' + soy.$$escapeHtmlAttribute("@" + a.sender) + '" data-isfaction="' + soy.$$escapeHtmlAttribute(a.isFaction) + '"', ">" + '<span style="background-color: #505050">', soy.$$escapeHtml(a.plain), "</span></span>");
          }
          else {
            c.append("<span ", PLAYER.nickname == a.sender ? 'class="' + soy.$$escapeHtmlAttribute(a.team.css) + '"' : 'class="' + soy.$$escapeHtmlAttribute(a.team.css) + ' pl_nudge_player" data-playerstr="' + soy.$$escapeHtmlAttribute("@" + a.sender) + '" data-isfaction="' + soy.$$escapeHtmlAttribute(a.isFaction) + '"', ">", soy.$$escapeHtml(a.plain), "</span>");
          }              
          break;
        case nemesis.dashboard.data.PlextMarkup.Type.AT_PLAYER:
          c.append('<span class="', a.atMe ? 'pl_nudge_me"' : soy.$$escapeHtmlAttribute(a.team.css) + ' pl_nudge_player" data-playerstr="' + soy.$$escapeHtmlAttribute(a.plain) + '" data-isfaction="' + soy.$$escapeHtmlAttribute(a.isFaction) + '"', ">", soy.$$escapeHtml(a.plain), "</span>");
          break;
        case nemesis.dashboard.data.PlextMarkup.Type.PORTAL:
          c.append('<span class="pl_portal_name" data-plat="', soy.$$escapeHtmlAttribute(a.lat), '" data-plng="', soy.$$escapeHtmlAttribute(a.lng), '">', soy.$$escapeHtml(a.name), '</span> <span class="pl_portal_address" data-plat="', soy.$$escapeHtmlAttribute(a.lat), '" data-plng="', soy.$$escapeHtmlAttribute(a.lng), '">(', soy.$$escapeHtml(a.address), ")</span>");
          break;
        case nemesis.dashboard.data.PlextMarkup.Type.SECURE:
          c.append('<span class="pl_secure">', soy.$$escapeHtml(a.plain), "</span>");
          break;
        default:
          c.append(soy.$$escapeHtml(a.plain))
      }
      return b ? "" : c.toString()
    };

    /* disable field rendering (if your map is consuming too much CPU rendering multiple fields, or there are so many layers of fields that they obscure the map)

    window.nemesis.dashboard.render.RegionRender.prototype.show = function(a) {
      if(!this.regionPolygons_[a.guid]) {
        var b = goog.array.map(this.getPoints(a), function(a) {
          return new google.maps.LatLng(a.lat, a.lng)
        });
        //b.length < 3 || (this.regionPolygons_[a.guid] = new google.maps.Polygon({map:nemesis.dashboard.maputils.map, paths:b, geodesic:true, strokeColor:a.team.color, strokeOpacity:0.2, strokeWeight:1, fillColor:a.team.color, fillOpacity:0.35}))
      }
    };

    */
    
    // add a div to the document to display Garmin POI lines

    var garmin_div = document.createElement('div');
    document.body.appendChild(garmin_div);
    garmin_div.style.position = 'fixed';
    garmin_div.style.left = '750px';
    garmin_div.style.top = '20px';

    // display portal info in garmin_div
      
    window.nemesis.ottaky_garminData = function(a) {
      var portal_title = a.portal.title.replace(",","");
      garmin_div.textContent =  a.portal.lng + ',' + a.portal.lat + ',' + portal_title + ',';
    };
      
    // add in the real portal level (this only works if the resonator data is present .. which it isn't initially, but it is available if you re-display the portal details)
    // also populate garmin div
     
      
    window.nemesis.dashboard.soy.portalinfooverlay.basicMetadata_ = function(a, b) {
      // send the portal data to our Garmin div
      window.nemesis.ottaky_garminData(a);
      var pr = 0, rl = "";
      for (var rc = 0; rc < a.portal.linkedResonators.length; rc++) {
        pr += a.portal.linkedResonators[rc].level;
      }
      if (pr > 0) {
        rl = pr / 8;
        rl = rl < 1 ? 1 : rl;
        rl = " (" + rl + ")";
      }
      var c = b || new soy.StringBuilder;
      c.append('<div class="portal_details_row"><div id="portal_image_container"><div id="portal_image">', a.portal.image ? '<img style="max-width:100%; max-height:100%; margin:auto; display:block" src="' + soy.$$escapeHtmlAttribute(soy.$$filterNormalizeUri(a.portal.image)) + '">' : "", '</div></div><div id="portal_metadata">', a.portal.level ? '<div id="portal_level">Level ' + soy.$$escapeHtml(a.portal.level) + " " + rl + "</div>" : "", '<div id="portal_capture_status">');
      //a.portal.isCaptured && (c.append('<div id="portal_discovery_label">Discovery:</div><div id="portal_capture_details">'), nemesis.dashboard.soy.portalinfooverlay.captureDetails(a, c), c.append("</div>"));
      a.portal.isCaptured && (c.append('<div id="portal_discovery_label"></div><div id="portal_capture_details">'), nemesis.dashboard.soy.portalinfooverlay.captureDetails(a, c), c.append("</div>"));
      c.append(a.artifactFragmentNumbers ? '<div class="portal_fragments">Shards: #' + soy.$$escapeHtml(a.artifactFragmentNumbers) + "</div>" : "", "</div></div></div>");
      return b ? "" : c.toString()
    };
      
    // add portal level number using embedded PNGs (*lev1 are not used currently)
        
    window.nemesis.ottaky_base64_icons = {
      enl_lev1 : "S0SURBVHja7JrNa11VFMXX2ufcl9S2IToqDkQ666DaaidNhIJTB8XqzEriQKoDURqRVkVQQUSaCk4sOGg68Q9w5D+QFMGiWKhOBEfpyI/SNh/vnrOXg3vee2lRWuFWXsi9cCHh3dz3fnevvffaO4+SsJMOww47OuAOuAPugDvgDrgD7oA74A64A+6AO+AOeCcCxzZuQvK+rltYmiWAwSkAWpxfvq+VS1ubGbZxo3sBL1yaMRbQ5lJxy1sLgM7Nrfi2Bm6iKZIwUiRBUgDBC2/9cGDtRv0UAEzsjlde/+LpaxIFQRL9n6LeFnBsO0dOX5wFACPVgJpoBGni1x//vO/31fUDcy+9+u7Jky8/DQDHnn3mjWD+i0S5UwC4sDQrCfn8K8vjmcN3hhtmBdaCaCYzyr565+qBIwePnX3y+KH9Z86cPTQ1NcUSusdD9CA3JyF3uDsA0AD4WAMvLM0CI1gLJrPgDObh9o36yPvvffDc0aNHJ+9oE0aL0YNnEKSXxuHusIWlGV+cXxnnCMuMgJkYTLTgFoNbiB4e3b/r6ieffvTt9PT03uePv3j4xIkXHgEAC7CqyjFRGdlKFTNQ8AcR5Vb7cClMZiYzcwvBLVQ5VFWOpz577Nf08PcffvfTNxeuX1+9NfwAgVZVOcbKQwgegsnMRDMRbN8nWHtynmkqsZVCFWQxeojRQ1Xl2Oul6vip6b8md6MSNCzrsUKoejlWMccYPVhwM5ORICG+fWmGYylpEgRFUjCWYmVuMXqsqhwnJlKvN5Gqh/aoR8MQIkSEqspRTrnTczYzuruRdFKCAchjFeHTS6UVNS25iXCJcghuscqh6uU4OVn39kxpgnDbAmxVlWOIHqzImQayPLzSpsZO0iw2BoTQgIvDSAdZjDn0ernatVsVOZJ0CLAQ3MycZhqANlaGwzvbeAFrJFEQHP7GwYsaRj5WCltzeJgOw4d25yPkWBYtYuT71LjVckqiXJRnek6WN9ZZE+Yjywi501X+DuUsP2/12+MDfL7xvhrBUhJdbvJsnpPlug5pcyP2125Z38UhQE7wlMw9013mA3gIQ/d8vxPV/1qlVfx9gZU7lTM9Z8upDqlvjYxv32QfW4EzvK5DSjnknOnujSIkolzXqvFoz2kJLtIkSU53J3O2nJJIhuaTO7V2E31pJNFcI9d1qFNtKSfL7k2UG2i0Dtxa9VucXwEE9xJdz+Y5m6ci534/1psbVX9zHWlrTqaEXPdDSinknBtpyyl5o5Rzc2PspSUKDnkzCAuZDgQMJR7MN9ZUEyNJe5bXdUi55Hr2YR77g5iWWvWqi/PLkuguenb6oGANolz3Q7257mmrpD3DUx1yGsAO8xg6N7fS+jduWp+HJQhOOcwHfclFmUmNqeAfq6urf16+fHlfaWm/pWTZ1Ui5FC1HE+H2x/UHseI5fXG2OCZYs/EYrngIAl+++eMTazfTQQjYtTdeee3zw9cG/RqClzXP9lviLSzNgM2Ix2blM/BiQjFbTStDAwrA/61IbautJQCUMY931Q0vG0vdO1W2GfBdkxUB6Pz88n+oDWMEvJ2O7n9LHXAH3AF3wB1wB9wBd8AdcAfcAXfAHfBOBP57AArm8a6AySUJAAAAAElFTkSuQmCC",
      enl_lev2 : "XgSURBVHja7JlrbBRVFMf/58zMtqWCIKKgKRoj2LIqsRYfWxQlUYmJojUmRJAW1EYKqHSrgg98xic1hrjUDya0EsVHNEQFUWr8AO02CviICMZnJaUikepGaHdm7jl+mN2yGnwlS7INM8kkm+y9d/Y3/3PP+d+zpKo4mi7GUXaFwCFwCBwCh8AhcAgcAofAIXAIHAKHwCHw0Qhs52MRIvpP4+Kt1QQgeysAba7r+E8tl3x1ZigfC/0bcLwtxpQBDYYq5TxaAeiK2k4Z0sCBmkpEYCKlVYu3Vrr95lxVnJgDoQDSxLSrdGRk2y1PV/aokhxO9YIFblxdDQDMrEykRKy0sn7rjPnz6u9sWNBwfjQadXLnplIpbW9v3x9vWrJm7749axcmztuuStpc12GOBHD+kxYdgmVLqeW27ZU3zau/M/FcYmo0GnVSqZQmk8mBZDI5sHv3bhkxYgTV1NSMfuftDQt9V6qYlYiU4q3VRySh5nXReGs1iAZh2WK13H5TtWBBw/kAkEwmByqrzk7MuGpa44yrpjWWR0+75/EnHtsFANFo1Lns8isufXH55+My0BxvjRVmls4JPGYCmJUsVmJLGMDYbBgnWlZuiRy3/9X7ExO/Bkh/6fX40UXLRy5bes9SAJgamxrdkvzgVGLdSwoBiAFIwQITgYiUmZWZhS1LeNgI69NFixs6KyoqTt66LfntlQtH9RYX+xEoMHY8CVs0GGXDhw8vAVDCrAQFGSlg4HhrjIhAxEGiYkvZtsW6a3X5h68++XLv+1u8cVfeOurLSVOKQOwWq5Ian43twMp9YZZDzKysQkpQamqL0YraTi04YCIQSIlIwaSUVdm2xZ7/0LjuomKvN1LkO47jFluWsAhp+yv9Iy+eOr08u8bOXTt7jj0+so9JSJiIhEgVDMAUVNJqbA1KEQUVKlA4o7JlCduOsZyIsYuLvUhpqVtyzPD0MEK6dEfXwcnXXTNrSnadbZ98/O0pFaX7iEGUeXkAKB6sX1AKU6aqg6AIwJUGlbaUbdtYkYhxSoa5RevbDk78aJOcXnPZ8llz5847AQCeS6z8sXvPF+8uvLs85XkaVPZDJjRvezk/wAoCDaLnfs58qUQE6v7KG7ZxzYELHbfi0mcfbpl+wQWxIgDo6koOJFqe2Th91pjkn+eCsqa7sBSmnN+kgXnL3KpKKkr6wgN95b3fpc+5vf6p+Q0L7jgp67JWPb/yu8QLj7SceErx9snTxh903YBQFYAi128XlrVsaotZzMqWJZZli+U4QQhHIr6z5tHuSvfX42pee2l9bTR6pg0Amza9d+Cxp5dt7Bv46q0b7h7bESkp6k+7juumLc/3LOP77PuGRQzLitpOP1/WMm9ZWjN2V5UCVYXUGJIdXQfs/n2jr37j9fVzo5OidiqV0tuW1G/u+nTdugnnlHw26+aTvk+nbc91LWMMiUgQEaoEKGnhGg+FKBGrqqqQiBAZw2Zj694zF9183yXRSYGPnlN7/fpverasXZIoSxrDJj3AxvMt3/fYNz4bERYRkgAahQvcXNeJpraYiBKRQMmwGAL9+rM3ec7sGycAwLp1b+79bNfmtadWlHzz0lP7x/ieiudCfFcl3a9mYtWon866eMwBFVKVIFJW1HYWrpdWJYVAJTgIKwyJ78kxZWVlDAA9e3p+u2jKzHmHcl1QaFWhUKBjQ8fOX37a82D1tWUHVUnyrW7egZvrOjTeWi2iAIQAMDJhCQBYtvTeMwCc8XfzZ8+ZLRva3zz+wpnju0WhzXm0lEfotJRRS0gFLEFtwg9dXV39mUM8HSbDD0L19fX9bjucFiVBoHD+j+tHosXTuLo6Yw3BqxZ/PMkbMFUAQEyHTEk2tYsOhrUd4a2LVp2343D9rSHTxGtqi3FGSM4MC7ZuIHZQykAKhfxTI29IdS0z4FlnnHtgkUzHUv99qwwx4L+crAiAPlPX8T9yQwEBD6Ur/G8pBA6BQ+AQOAQOgUPgEDgEDoFD4BA4BD4agf8YALYN+bxeylEhAAAAAElFTkSuQmCC",
      enl_lev3 : "aNSURBVHja7Nl/bF11FQDwc873/nodvnY/WMbabpWtm+0DZe1rWkeMC2RaxkT7ttmgCSCaSQgyoziHdu1bUWOCM4HBkgX5IWhUtq7WQYoBAi44CrQd6F63jBUhHbLZ2O291dd7373fc/zj9W2FmIDJM75m95vcf27u95v7yTffc849F0UELqZBcJGNEByCQ3AIDsEhOASH4BAcgkNwCA7BIfhiBBvFWAQRP9JzXT2NBAIOAFr5ieIBgLdjwxB/lPnF6M5gURb5EHByf6OFKDYA2IiCiIAXDCgi4ImAl0wM52YtuHNfHBHFJpLIvp//bdl7b2VXeVO8BERsAAAkRGERMjBTPt9Ktay/9HDs6nlpEXSZwe3eOCSzBrz9ySaHFEeUYvXwD463VM6ruzHZ+eMbmptb5kSj0fc9PDY2xn1/+P3JXQ/u7Bdnouem5PLXtSbNGrPdmwbdkgd37osTopQrxYZhsLr360c3v/Haia7q6moCAEiNpPxMOs0AANHycorVx8zC/c+ubdj+3YfqHtcBaa0pEIGzM893Md7VKHYURBSngO1/5N2a9a2JNQXsHVu+OfjUs7982jAxBwCgfbGvX3vzugfu2xOP1cfM69a2tT7zyPN/ar218h0AAK3JAYBsyUXpGVEYicRRSpRSbFg2KEBBAIBMJiN9/Y/9ufu3iw8gCeV3DLmz/bHyn9xzb2M0GsXmptXLhp54+uOG0u+CAIhApNhgKu7ugkUkBikmpVjdsHnB6aGjB37Ttunzf7zlGxufvTxmvWnbgT3jcioWUDqVOpKPzghoWqCUwYoUE5EYO3ob7JLdYUSJEAkRiSLFShlsbLnvsoHDLx5+23bEam+NppUR2ESsEACYkVnz3KqqahMAYHIy40XmkK+UGKyFmYSZpQwAvJIDd/U0KkQ2kASJmJQSRUqUMlitXuekLUvblhXYmQl/zok3/PmTGTFPnuCK6kX1ywtn/NXBg6OfiDuniZjyQwgRzOT+BjOZGPZLbYcdRCBCIUJARCEiJkWilBLDMLSRmfDnPPzDsq/V1TYtuaRsrn3NlVfOX79t46UAAKnUkSA1enDorrsqz+RyQkhC+SJFEABtACgdcMeTTQjAJqIAoEDhRQkFkRiJmEiJGv2rv6CutmnJnl29q96Xi0+O8S23retZcZV9DCk/98IFAIDW9r1xBAAplaBFhbUQAAEBplMz4nSaRhC4bCllT42Pntm952fjvX2/m3zllUM5AIDqqmq6+zs7W0+PBQunM/rM4gQBBD9w7/9feHT1NH7MMHTENNk0rcCyLG1ZdmDbVmDbTmDbtu/YduC893Zu3sv93vJzZ6XszDg6S+d9ofmh3T1xAIBE+9rn5l/xl5+uuqbi7zlPeTnf8ANf5XyfppKJ4UxJFR6IkANARy58EIgwMjOy1shBQIFSyl98uXnmy3eqw4gAWpP+3pcORDKZTGM0GsVr16yve6JvYOmn1sw9eX4NAUEsXpQuWh5OJoY8ZmRhnIYSa418ZMAr+0Xn2U/u3ZWtyeVUzvMMz3PNKdc1Xc813MglND4yks/DIgBBAJoZmQVZGFkEuattuPTA02OKL+yqfv1gtmxw/+ItG679UUdtxeatv96ZrfE803Vdw/Vcw/U8w/M9DOT8YUXRATBr1KyJmZFFSrjSEgE3jyXNmvRLfenaTYmbmu+4fUvlV9pvrk0NeMsz/yTf80zPdU03PaFy586KU1WZLzwy5zIuIma1Jq0ZNTOxCLolW2klE8Oc3N/gMmMk0ISLapx3Xn3t0AmAO6tisSuMW2/saO/6anfF+QaAAHzrts7PFQqP/mf6hupXl78ZaApYk2ZGt6ttmEsWPP1BMKU1WQCAK+Plp154dOB4aiR1daw+Zm7b2rFs29aO2//TvAcevH9sbHyk//rPLJuY/jzUzMXd3f9ZA6BzXzyqFJtKsfGre966Sv41N3H395NfXLliZXlLy6edwnOpkZR/7NjRyRdefP74U889vntN+8KXVzRWnNOaAq3R37FhKD07Oh5740gkJpFEiMR6qffUgpFD6VXelF7oZbmqUEIYFo47Zeofi2qc0Q3fXnJ8+vx7IugmE0PerGzi7ehtMBAhgig2oijM19mFIAf5fIs638jDqa624WDWdy3zncsGRAQbACIzsgMDgAsAblfbsHx4fJhF4A+ccQMAsHvjoP/fBcQSAc+mEf5bCsEhOASH4BAcgkNwCA7BITgEh+AQfDGC/z0AaslS56qeDAsAAAAASUVORK5CYII=",
      enl_lev4 : "XwSURBVHja7NlrbBxXFQfw///cO7O7tpP1g5S4aay0SiAoqZ20UgkKEaW0afOipUJFCAVQpIJUIZUGVLkpIEV1HkQCvvCpICQoVSGq2qZ5obZf+hBEKm39CETGUBUSqQnBj7Udx/bOPYcPM3YsUYkgGbEmM6v9tHvvzm/OzD3nnqWZ4Vo6BNfYkYNzcA7OwTk4B+fgHJyDc3AOzsE5OAdfi2A/H5OQvKrv7T/WEQFoNGARABCoABjds72nejXj56M7w3mZ5N+ADxxvbyCtkcRiEgJmv2mAGc2AURhHOrf2ji1Y8L6j64S0soi2iLOiiDmhOYrJ+Xen6l59buhjg+9Xl5rBNJiuWFN/+u6vLP2DKofMOLxne48uGHDXi+taRKzFeY2cU+9EI3HmRUxIc788cH7Dl7btfeTWW25rnhlz6Id7X6tf+cdDHZ9qPB+CVFXl4uM7uofmG+zne1HYd3SdJ63F+xA7r5F3GjuvXsQiEXNvvjR2wyKu/PQ3Hvpm69xx5cXlhukkafCRFkgwSbBk/7GOsat9vv9nqzTFmpzXyHmNvNeCj7TgvRajOBTjOCn1v3Vpzbcf3nc7AJw9d3b2thWBFEqIswsVO68xxZpqOi3tP9YhItrsXRpZ7zX2PsRRFIpRFIqnTo6uWr1s852b77qnBAA/+dmP3rtyJurigs2O8U4jEWuuaTDFFjmxWJz6NMIhTqMcClGUFAfemVy968u71wLAcy8cHr9w8b3x2RMhJC5awXuNnbNI0me/cPDkzY21C6a1iJgTMS/OvHOWoUPhpafH1n6y48G7P7FhYwQATz3b1SfO3JyLJXFskXMaO6feOfPizAnRUpPgA8fbCyRKFHNO1DlRL+mJRxZC4d3T06se/OruNgB4/sivJ5pWDPyFMgdMEx+Zd069iPrZNEarO3iiva7mwAY2kSZCEwoc00g7cepO/OLSmh23P7pp+fI2AsDh40/033YnR8TpnAiDLk1dTsQcs5xNmgAo1xT4iSPrBYZ6EgSNpJEESZPhC0nd+/2LO762a3drGt1nLl+3qv9vpWKokxQzk2QpLgWSENJIWDqjodz14jqppQh7AA6wrEQGCCMAvPzMpdWf3/bI+nI5DdLhY139GzbzH86rA41zI4yZ0TSA6YsAsrqmdsDfvfedaQATMFpWvJkZ7dxAta46tPLWh77+rQ/NPLvl5X/u7zvFujNvS8FMZvOwKmzwvJbMABgN2RyWXsWJ73y2O6mZ3VK26IyaodEMakYzo576zUTbljt2rrly18I6lnZtzmKJpRvbCjOf3fOZL67o7r1x1yu/+vmPN32u8YwZNZ0LhnRXhZoCP7atd/T7J29OTKlmDKpMQsJAcrYAvv++L9QDqP+g8fff90DD9a03xA9/78m2jffytCpDima1c2tv7YGz3d6gKgsaGIIwueWO+oEjzz7V83bvG0tIExEQMKGAIHBTW3vzo7v3tgLAoR/sPff77tf/ump96Uy2eQgaGMw4WHMNgCtijqjKdUGtKmpu2crS8Mcf+PuTlYvnmqLYYh+Z9948md7UA29yJ4BWAOgf6LuwpL3vp+2bGs8mCZMQWFWVBIbhmgV3bu2tHjzRXglBmkmrAuD1N5WG2j5SGHde47QYMUeadL82ucSMOicr6fSkTCdJ+g5BqkE50rmlr1q7EQagykEksoiw2V2yGU2VGpw4ScyRxsnLclkDwyxYqVNTSLGJmw6JJKYcnu/zm3fwnu09l/cd7ZgAZWbPbqrpIiYhq6AI0rmxymhl/HenfjsFAJXKyHh5mRtPEjcVEqmGIBN7tveMz/v29b/U8RARaxCxFnHa4NIS02clozAtK/Dy00Mr+t+a+KgG6I1r607ftfPDf9LAMVUOP7btX1fmBdHEO3iivUhaC8XKQvNZE4/ZIocsb1fVUDHjYOeWvskF37XM4C7bBDQBFoMADFUQQzBWOrf2hv+bNu0H9L1KAOTxHd2X/qOsVyvghXTk/y3l4Bycg3NwDs7BOTgH5+AcnINzcA6+FsH/HAAlv9VGFDG6VQAAAABJRU5ErkJggg==",
      enl_lev5 : "ZeSURBVHja7Nl9aFvXFQDwc+697+nDki3ZTt3EjhonTa04aUO6ZJCkjeN5FMpYRyEJCxvsg4yNgp1kHaP7oIyxrDSwdO76Rwel/yxjI1m3wRisrF6akbYJtPTDcmw57mJLWJad2JZsSZb17j1nf9henHQfGTjwTN4D/fXeuej3ju695x4hM8PddAm4yy4P7IE9sAf2wB7YA3tgD+yBPbAH9sAe+G4Eq5UYBBFv67nuc3E/AzYCQ8NCIGcQIHu0faB8O/Er0Z3BFRnkv4O3Nm32b7p3g11fXSsjQoBAsRDAxEwEPDNlpidSlclUsjwEAH2rEvzC37ao0ydG902NO0/Ml2gDCkCBIFAA3owAZgImBgpUiRHLJy5962TszLH2fn0nwOpOzJNTb7Q2I1Kspl4+nB2Zbz57uudxZkYAgFtfzXLCj098b3As/75JJ+dmX+jZ8uHxjv6UK+fwLZn1I1JMKfKVi6ZOV6h2z+691u3ERiJR/3gBAqRNUEje9PNzWyaOtfeXXQ0WyE1SkU8p8lk+tqQEuXTvzNnflP/4p9PXABcTzbDwI2UkBoRE/3u5upiYj7UoRynyGSPWA8AV14K7z8WVkNwoJdlSkq0sUChvbH2ZTMoZvt4zUB0VRUBAZiAmYGPQGA2mcYuYaYjZ0+EoSO2QzQxNrgajgDVCUEBKtqQiWylQyzPMwKgUq72fs7IADAzIRoOuqVMFFKIcbbBylq2KjiabCYkIA7/4e8u6zn3JjDvByDEhWAlBSgqylMVKLsswIqDlA+v8H+YeXMgwEhkgX1AU72lUE/Xr9PVPP1b1MREakqSFEYqI7wMA94G734yHEKlGCJaLHyUlSMQbC/PY+LBTY+3Y2Lb7iejiCs3ACMmhj4pvXXhtJFKvJ2ZyLNsPhPsEoRKCJKKoefF8S6SrLZlzV4YZmhBBILJAwRIFy4Uig/+V4Z07Hg0+f+LlyL+JDib6eus6nz70j9RAqnD5UvnaA5+qSqMAicgCANcCgHvAP3ujVTFQHSIjIggEEIiAKFgsz/DBA1+yz/zutJPNjpilPTgUiuCBJ7/s27b1QfHUkWfXP/fS11KppLOmZSePIjAiMjLDvad6Wq8AgHZLhn03jbVQOH6iADv72q8rr/7+q4OhakZAQO2gnp9DM3ilt/n5Ey9HDh447HvmR1+J5idN1bKRcPGlSdeAn/7s5WL3uXieGQIL8xIYALm6VhZqG+Rk/GGrr7VlZyQ78XFl8y6R3/0YTAMCGi30hT9zbWZ8pLI0VtO6+6vKpZQ1NW584agsASADwPS3Oy7PH2c3zWGELDM2ACMxIzEh7ezwp5XN1tysuVYufRCOb2TYuFXOte4SU4CAmaugjNbVim9UmI1rm4Pp3IiVv04yFEFiBgKArOvOw0f3D2SZcZ4IiRgNMeqhj3Ro8H3dkLio11/tM3UTo2BbPll0HFVyKqpUqYiydlAzI33iMEFAxGiYsdLVlhxzZ2nJkCLCOBnUJFCPp03g2qiJvtLds4uZsev7He+NDYNwHDkHAODMIzmOdnhZhtOjVwsqIkqhiCoSoWGGYddWWgyQIcL7DaEWRjiTWfIX8hzcs/sRBQAQtjdFhxIjoV+dnN+ACDgzzfZklu349mbf0hjDI0MzLfX+UmSNXXQcdJhx1LUtnq62ZJkIs2RExRhR0RVwSINZuv/UN55dDzM7Hrr4uvPAO39xNlNux/a9249s/+EzP60DAEgkeo2ysOAPyhljRIVIjHXuS7r7tEQkUlrDGgDAQFhNh6Jy7JVXf1k48vVvhg4dPOw/dPBw83+K/clzP0iFIjJTFVGTWmOFaGWze0eaeMfa+3PGiFmjReW+1sBwda1Kn3zp2Ovf+W5n9u23L+h8Pn/T84lErzlz9rdzj3/+0YF3E399tyHmG3zkC9EBY+TM0f0D11f6+92RFs+pnlYlBNULwbH+S4XNvW/NxqfHdVMhZ+5xKhQic+MEZdlYtHyiUF0nx+ob7fS2PeGLzduCyaP7B7Krson34vmW8HBfKZ5Klh+andL1pVkKGc1qaQsKhEQhGJY5ZYuhji/WvtO5Lzm72ruWS3AFAGsBIIYA/sVVvYwAaQbIdLUl/2fZuKrAt/S9agBAHf9M/+T/te25BbyaLu+/JQ/sgT2wB/bAHtgDe2AP7IE9sAf2wHcj+J8DACLyFAC24cPlAAAAAElFTkSuQmCC",
      enl_lev6 : "aQSURBVHja7NlbbBxXGQDg/z9nZr1Xry9xfEu9cW3vLvG6JglQkk1S1LRIoFalUlNEi1AIT3FckEAqUPGEoEQNUmlStwhBCm+obZoiRYCgdRvHjpPGIWmatrY3OL5f4vWuZ9fe25xzfh7sBScCNUiLWCtzduZlzkXzzZnLf/5FIoI7qTC4w4oFtsAW2AJbYAtsgS2wBbbAFtgCW2ALfCeCtUIMgoi31a6r1+8kgiYAaFg9NIIIY4d2Dadup38hsjNYkEE+AfxCd3P92VPGrlxWBZMx6ZGC/nmhlQIlTBpfWlQfvd+zdAEA4usSfKwnoDNGvtnR7LaLbye2zU+ZdUZUVGdSqlQJ4AAEAAhEQHoJpjxl/Ia7XLvevsf9Zmin+2Ln7mFz3YCPvhvcwrgKGPPZsp43Fj89eS3XHGq+v23/1zuqd3w+bPN6vTe1v3r1A3Xi5O+NV08937fJXzK492sVF50e/dJTe4aGCg3WCv1SONYTcDKmApomHVd6l1rmxnNNX7rvwGePHH6xKt/GMAy4+uEVCQAQar2Hh0JtLBRqK5+eGd9+eeRkMjqZHW8M8bYXz/gnOnff3vP9fwMzRs2cKzsy5UwnRUUiJqt/9MOfVgEATEyM06NPfu5aOhPLOTyYJQWUSpLttd9dbA21trGgv9UzMHjCu7SYK+fcHgdgLQDwftGCu3r9OmOqSdOkffZ6tiIZFxvu3fZAXf4WfvbI92MV9YsLjVtsMU8Z5giAzvwh15gwFlfuVQTQNNB0G9k0TZUQYXGDEamecXIxTrZMWriVINemOp8jX9974fV4+Mt6KnQvy+SyiiuJVH83pn9+7Okbrt+U85HZdyJ1d2uJGh8XjCsbV+h6+Wxz48Gd164XK9jPmdI5U3p2WbqEUPbamrtK8vVcA00K5ew+ka2SghgRkJQoNdelibSNpe8Ja9HgZ+wT1Q08a5pkU5JMRAgCQPGBu3r9XkRViYw0xkhjnDTGgK19gXNObOZjX9OO7Y94PG4vrn6WYGZuzOzpfzMWnzfs6SXA8EM85fSyHGNMQ0aVL/W1VHWEI/PFNsPNyIgzBI6MOOPAGbs5dD20/xc1B/Z32v5N3xLDeM797HM/8J7/+LfSXZYxdj6kJZARY0hcAmwGgOIBv/BuUCdStQiEgISIwBgCA1z9rZYD+ztt/efOqIG/nZYAAARAdTWb2YN7H9a8Xi888/Rh74OPnKy+Mbk8l8uSzhgxQEIA8B09HbgMAGaxzLADEXRAWN1WObeUb3V8MTFtdCcqq4kQAYVAsXwBaeDyNzce/vGvXF6vFwJNWz0xo8d+Y0I4anya8a/xQC8EuCCrpe98YTABAFEgBAIgICAipFvNV4a6k+1hFt/7GJ+7/zE+98A+PhfYivG3zhyP5dtsa9/jTsSUMzolHSvBFRIARL9931CqyJ5hGieCBiJUKztIIlCwlk1AUqDp38piCIBKofjwgtygbm4DSqFShESEigAUAIwW3Xr40K7IGBFmSKFQhEJKlEqBWjvJUoA0TTSFydOmyVNC8IwwMackqjVxOSkFSkkQilCSwnRHOFJ84JXgHgaVQqEkCruLpzQdMzOzE9l8vZAozRzLmiZPmYKnTMFSpslMKUCsGQNIgZIShJIoiHCwaCMtIhyVirUzpXJ2J1/mGktPTY9l8vVOWwUuzCbh1S5zI67glBA3z/Bg5IMl3c6Sngo9oRTLEcBI0aZ4OsKRZSVxVAqWrfaVxEor+cJ7l96aMQwDAAC+2/F8/fzfG3x9p8RdvSt7Q/+fzIavfuV7tfkxzr3XHXV6+GLVppKYlDh6cMe15aJeLSmFw1LyTYjAXKVavLSCz/3kZ88sHDncVfn4vidsj+97wvef+v76+C9TpBsTnnJH1O7UlqRkI4U+v4KDO3cPR4/1BOIABG1hdyQ2J51/6XuFTX5jrPXRh5+s2vKpkB4KtbG1a+P+c33m2+/8cfHPPccHyjZqUw1Bx5gQLN65e3i60Of3v8l4nA7ojFEt5xScn8xuHvhrwh+dFjXGgqjKZciZTZMjn+JBBsLuZAlXKYtX1tqmm9od/W1h9/lDuyJj6zKJ9/LZljIlVfCj86ntRtTcsBSX7uWkcuYFTGPC5eELWgkOKglnXz8anVr3WUsAgJf6WnRAaASCFkRyrR5eBoAIAF4/uDPyiWHjugLfkveqBAD9qT1Ds//dZ69IwOupWP8tWWALbIEtsAW2wBbYAltgC2yBLbAFvhPB/xgA1GJH4qX0ZCQAAAAASUVORK5CYII=",
      enl_lev7 : "ZPSURBVHja7Nlbb1TXFQfw/1r7zM2e8RnP+EK4FNk4KRGXGAVsoJDeJFpVTUraqmofkrZSiBP1O/QLpE+VWpJGalGlvqVV4ohCUQEVAyUkKqSgVFAYggQOvs+M53bm7L1WH8YWSirRVDLSWJwtrYeR9to6P+295yytQ6qKR2kwHrERgSNwBI7AETgCR+AIHIEjcASOwBE4Aj+KYG8lFiGizzXvjQubMgCeUGDLUsYNEK6Mjd5c/Dz5K9GdoRVZ5MHgxNd+4O/o2+Dt9PNmA7Ey0dLJUqgISbXk7i3MuA+O/rb4PoD6qgQfPj8UP/9uaWdQl28tTIWDQUPSzMpExMTaShCoKlSUJJ6kSiptJju7vImvfN8/Obb7v3d9JZ7Vexj35NfnHh9mlmFAhycLwWBxxvZVy85nBhOBPo2AqpJ2ZKjckTH5jU9SplKyGw//fegfr+65MdGWd/gzO5tmlpFKMcw1G7a7XnH5tfmnBn/+i9fyD8p7609/qJ26cOSabbqFarGZ7+o2216/sOmjV3bfnGtrMBvd7BnpAGyHMZpi1oSf8WNf2rv/gW+EC+9NxFQ1CdVOsZLxPCnD8jCAk20NNixbjCfJfB+Zrhyoswt6vXC5+cMXDiwSASD91JF+/ZdvpX3fh0LJ8+B5MY15MUkYzyVFsf3Ni4NnD40UgrYE/+bi4AAb6TFGErGkxrt7Cb1rOUykSjOzwd+SbNQwgYlb97jb+3Le930AwOmJd+p+nqzfQ8j3E4yRpAhbVX0SwOW2BDNhmFlixkjcGIl/cYdpdHR6i40qaTOQ0PM0xkbN9F1JfnRRMj/9yYsJALh69Z96Z+pSY+gpz/asIelbTwhDjYuTmCN6ui3Bb7y3KUMkX2DW2HLk+kj71pkqBGjUAC8miSvnJVtZ0A4JsqlvHHjOAMAf3/697e6F9XPAmg1kl/OJNcakuTcvDg4eGikU2q203EakhkkNkRpi9YjVY1YvnlIv1w+qFjVVnJXUYlHi+0cPxpeP87FTRxq5frI9j1G4ZiNbYvWI1DCrIYYBsLn9amnF4yBlEJgITKTLYZjATGrmpiRRLWusNKvx73z7RQMAx46PKyeK1s+T61tPtisHuZ8LJlICYeDw+aF024B/dfaJNIAEEZaqCiUA939CqVJWrzjbis7EgLd3zzMEAKfOjDs/B5vtge1dy020cj4bABBvG/DP9l2vgHBblVQBBUihpArSVkVIMn1HzewnMAsz4D07DxoAKJVLOHn2d41cP4V+jpvd/dRsVdiQpUpSoFAAc6/uvTHfVkeagGuqEFUSVXKqcCpwImRVyc1PgcrzwNwU6LvP/ZgB4Phf3pG0T2FnhoOe9VxLpjhYmr+8hqiSEPTDtrvDL4/e/FiViipkRWBFKGwFh8VZyMwksDADZJID3tYt2wkATp8ZD9M+BZkcNbI5rotweD+PrCpZUaqqUqH9/rQAqNIlt4QU4dAJN8VRMHkbmL4Lmp9W2j/6vAcApVIJ5z54u5bp5lpnF1ezfd6icxSI46YTbopwUxyHqri+kpXWynY8FP8Sx03nKHCOA2e5YR0Hs3dVS3PQ+WmY7x18IQYAx0+Mu440NeIpqmd7vDIxV53lhnPcEEeBc9R0Qk0VutS2LZ5DI4VAlK44yw279PBzU2SnJ1u7m00PxLdubR3nk6fHg2QnVzu7zGI6a0rWmrq1pmaXc1v5hZd23Zpp656WOLpsnWk4yzUbcu3ex6KznygVZ2G+uu9ganne+5fOVOJJrqbSppjtjc3b0FSs5VorTM1aUxehq23fxBvbfXPOWb4VWlMLranN3oOU5hTleY0d+PqzCQD487F3XSNcKCdSvJjo8EpgrxyGXA1DU7WhqS7tdPHlkcK1VdG1fGXPjaPi6IS15t9BnWrNgIIwRAhAz52bkBN/PVqLp7icSJl5vyc2HYZmMQy9ShiaSmjNbefo2Njum0cexrM99CbergPpHS6UH1WKMlKvSI4IlM5yNdPNi4PbEnf2PZsupDKmDsUVUbr60s5bd1d91xLA0/Ekbe5dFxvs2xBbl+01prvPVNYPxQvb93WeUeDDQ7v+96tntYCX0Y8B8AHgmef9xOg3M6XXxu78X3e0bcCraUTfliJwBI7AETgCR+AIHIEjcASOwBE4Aj+K4P8MABCaVihi4oyEAAAAAElFTkSuQmCC",
      enl_lev8 : "bcSURBVHja7NlbbFxHGQfw/zdzztmzu/Yeb2w3F99zaaqEJnbiJMW5tCqp4kIJlYLKQx8SQt+QKoLEU0uEFIHEC4iCeKBtmqhCIEWWStWSCHrJzaHEwZc2TQKt2bUTx27c2nvzevecmfl4SGyKoCCEkdbKGWmf9syn/WlHM//5DjEz7qYhcJeNEByCQ3AIDsEhOASH4BAcgkNwCA7BIfhuBFsLUYSI/tMjywHUNqyyGzoeim+uqZNNWrM/eUMND18uD6Qul1MAxgHof1dkIboztCBFPhvsAVi9/gH3C9VJuUFaaGRmi+YmMLMbFyW/zO/np8y5wbOz5wB8tCjBDz+RqJUWnlS+eTCf0c2FjEmUZkzUGAgizE9w4zSbSMqCVy+n6xvs4aY1zom1nbEz39icKi0a8PMXV37xw6Fi9+ULM2vGhoNGXUzUP/OdH9V273nM8jzvH549eeo1/dzPj+TGp4cmG1bb42va3dH2XVUpremNp7akzi00eME3rRf62pqlZbbmpoJ6FXBitqC9X770+3u+9sSTlud5yGaz6O09y6OjIwCAR7sfk79++XdJBIlkaUYn81NB3UzOXyIl737xT21uRW5anx5S8qZiPqjxi8or5rS3qqUjcf/nNhAAfPmrW2ZSNwYDNwoTBBCOrBE9L19KNDe34PEv7U+cOv/TQuCbxGxBJb1aWVZKdAF4q2KPpaP9ra4QZltpRnlambgOTPS+1RsjANB74SxPTA8GGz4vSu07ZXnjdlEmO8PXb4wwAFRXe1IIdoQwroCJWdLEpOCtFX0OC+JNUhpXBzqmFbvGsLNiRfP8KpIWy8QS2Nt2Ey+phxWtgrwxlr6zDwBCQtoWbGmZiJDGFdIsOz7Ysq5iwUS8Q0iOxKrJilVBRqKgm+NpAwBNjS2kFSyj2E69r+NaGUcHbDU2tM6BSUqW0mLbsjgipYlIyRESvLMiwUf7W+8jwfVCsB2rYhGNgyJRYGIyzQDQ3NyCiOXJsZSJvPuOjqWuGlcrlk1NLQQAufw07AiE7bB0XLbuLG+biO89NtBSU4mbVocgtoRgmawXpqoGXLtU6Kv9p/3R0ZFIc3MLnv32S/a1vwyCwQQADctb0dzUgmwuiwsXf6NqGoirk4TEEgIJtohgEbFkoAvAbysG/IuLKz3ArAVBACwzkzoa+LCUYplIkul55bg59PRh0b1nL3Xv2ftP8y9fHsSMShtPCDBDlmfZduMQRCyIWABof/7iyjcBlCvlH44QGEQACJgY1e7YsI5+NGoiW9fvjx96+rDI5rI40XOc84Xp+ZjCAPY9fgDbux7Evj2HI6++fSSIJ4xTnVSuV2fjdjUQAPfO538GL1jSOtrf+i3HUa2Oo2sGzhRXXrtUXvHBkKo7f+pjz/M8fPPQPv3XW68Y2/l7tGQGZ295OPP6lASA3XtXz1YvvZ5Z2+ncat/pXrcca8r3rWzgyzMHOkZ6KippEfiPzGRy00bkMywKOZZtje3zUXLoymm1tIlU+w4RtO8g1b6D1D2NpAqFrJmr0b7+ISuXgShkWWQ/ZsFMmhkaQH/F7dIMGmCmvF+CVj6M9sFV8eR8yM4XMgZMqn6FKG/YLmcbVomyZZOyHTJzMXPFslbhl4DSDHi2CGMMKTY0eaBjJFVx4IOb0iU2eM8YEWiNQBvSn16BbMhoTUopKl//kIJiQZSFpEBIqLm0BQZrDa0VlNEUGEM+M52t2CxtmHqFEDuZyQcQ5PMZNfdd3ZI2+mRihPveYkF0+1gqFmDyWWB71y4CgL6Bs74bpZIbl8VotSxoLcrMuFSxSevrHSPjsYT1XiQq844rin8eHixms1kAwJFnXoxa/kZ3qFdHhnqNM9RrHJPZGH320NHo3PybE2lfSCoLKYqOK/PG0Dv720dLFX1bMob6onHrAduRhXhC53/83A9qvvfdH7rbu3bRq119sc88y1/4mcqVRjJNjXYuEhU527XyWotzFX89PLg5PfT9nsYPauqDRD6j3Z6TP8GNsfTyRx7e665bd7+cuyreDhzv8uj1EX7j7dfKb/7h2HgiKfOJWmtqWZs7rpW4enBzemKhf9//p+PRt9I9eWx6jxD8lcmxYE0hoxOlGRNXATt+CfbtyAE4LilpkYpViWJVUhSWtzi3vDrZv7W7+lcHN6WvLcYmXvO6be7OpU1WZ6ya1vsldsslE5nvZ8UosGwROBHKKoUrY8PBiYHTswOLuWsJADaAumWtdv2W3bHOmnrZ6USQAAA7Qp9MpNX5wXOz/ekr/k0As4u9Tfuv+16X2u4FUH6qMzXyX4WbSgEvphG+WwrBITgEh+AQHIJDcAgOwSE4BIfgEHw3gv82AOaZOpAeF8onAAAAAElFTkSuQmCC",
      hum_lev1 : "P8SURBVHja7JpPiBxFFMa/71XPjMlmkWAk68HdAXMJXiPmqAcxN/GyBy9mZ8GAB0GEgDdPnjyLCJmNRPSmJ/UQxYi36CUJGFk8zOYQI4oICjs73f2eh+rqP3EVF7u1MVXDUAzdVdSvvlev3nsMzQz3UhPcYy0CR+AIHIEjcASOwBE4AkfgCByBI3AEbguY5MG+0x0edExbjW1UPP5qQfX5ubUjANh4vLGmB53nn7SkaxPi1g5BelASYAMYvHhLABgALfpu19OJwhdmAUy8TYaPf/H8o8vD9bXDRwHg1Ec/3CnlMyjMPPhk3InC7QNfmBGsgzb7l08eOfT80o+TLz779CQAPHv2hXdWP7h9FQqDmcJgMDMYFJtj679J1yGFHlz8Bnz51IMnHj7sHnn13Evr77976TEAeG7z3FUIvwZh3qjNoFTA2IWJtws8nQGklLC17+UnHjjx4RuvvX75k49P3bh+7VgYkhkEiSRQ885LoRATKIHpTO827b7dw3+EdXRImCwP5dh329urAHB8ZWUeBuypOTg6CENfPw6txwntThjUDYt1BYST5K1bu7MX337v/JtXvnrlyafP3AxDFgaHRAZIxDU2yjsGwXTWU+DprOacSEihtBOHhMnFO4v5mWu/fbM911+ASrnU4DCQpNwcqW9a+ypLq+rWHVfDpCXBwA0wcqNfyZGJlO/ODQmSAlgowcGVwM1ApSfA0xmDf256aXjohA4JBxi64a7I0FgBp4CDY7LP+WXBymL+XinMqiMapu3vZIETQcJkVzBUqawhBwRC3nX+uf/8/cyW+Ce/CJAGkn8XiO2ac5vAVnVmvisiJh87KdQUuWYjtZSqeRjoUISTZZRVjEMxB4rbuVfAk3GJWMaBAdaD5sg0xSJfHFGbMwQZAAZEjtxyKLQcV0Ib9our+xFpmSlIaaiklkNNkJmAmgLAEmwBzUvgIZAj1Qy55tBGLB020Ppo0l7lcrGm0GDG5tVNNcVevlgGFkSl8IjIkGmKzAql66YNQy1f7mPyoDDQn1pTEESmOYx+8Uq932HB6mxiSOTIzCscgNW0OMfattNqF9hMAQpqZ7R0aqYGhS4JMphWCgtzZJqVoAHW0Alwu9fSZIxGTluatHmo1NJlx6x+1ySEf6eurhaee2MN/Va4vEqKnJbGUGaAkaDxPqHWs/mBwHvx6hoLzzsp97QPvDk2TGc5zFiYdxVAEPx5L7/9+OnT11Fk+IscN2uOyis7GXdW2+q8aomtHRZHJyQDuPHMQysrh9xxAPj8+/m361d+2gWg2FizrquW/02ZliTMDKTZ2dV/tUzL+E+8CByBI3AEjsAROAJH4AgcgSNwBI7AETgC/+/a7wMA5xcz1zV95X4AAAAASUVORK5CYII=",
      hum_lev2 : "V8SURBVHja7Jl/bBRFFMe/b3bvrnClLVTslZIWKQIaDCiN9AoWNCABwYCKQdSmLTYCooJYEyMJMdGY+AcRjWiA3vErEgSJRIn8sAEhNIgWSoVaCugdvypNKe21B9fe7jz/2LtrG0oicZtcw77NZPePmbn3mffmzZt3xMy4l0TgHhML2AK2gC1gC9gCtoAtYAvYAraALWAL2AI2C5iI7q55/HS3Y8wSMqPi0ZNCPc1LXr8AQN26FWXJ/zKvWZWZXgcmr59A1AlK6N6ZwQAYgOTCTO6bwGW+KJgwfNJ4Nk1KHZCTah+YZBODol2bO2T9I7vr/4nASzBLLsri3gJWTY8KZT4CdQUlOjnLNSSjvzL6RMXhvJ3bKkadra0dFu0+we0+HShZdDgQlmeH7rhSCRCR189gyIjlzRVm/t+tm3h8Al6/io1+GzZfdJTXhzKqauvezXW7L0UAbmtOpzO85su1ZZeDWi42+e3w+lV4fMJsPZnZZJf2+AAiFQSCIAFBwj83feLCOTO//vnA/mzDonlXhmePqAeAYLCtX/n+faOCwaDqdDq1y02BkoHbr2wDM0NCgllD8bC4dmnRFRaChE0gvbr61BAAeOnVgsp3Pi/7pDEsm8GAU4E6+duNC1Yseb0gGAyqm8vW5R98rujQk/sb6kFMAAkAMn4TDyKBWIgiAYUUBgY0XLvWDwAec0+q/a6h3TfjZOv5GVWt5/J/C5yZXbDwRHT4sYqKMa5+SjoEiYjbCHh8pqponoU9vliQAhFBGJb235QX6tq0ZQf37H74vimz9pbWBANwKA4ADJ31MEPpsjUoJLnLosF0K6umWjf23enWeUdvVMEmal4ZM/3I1rpbN5GgJkCQALOExlqIO3VgADpHF40NYAbFn4U9PopgdrEyCALGXlZJ2doYDiJBdcChOKCQCsnyrRSRuvOrNfnRaXLcE2vOBLSG2BzGMlBkfo6nPUydL0I31zbOZAFFCKikwiEccKrOxS77g5OT1Yd++GaLGwCcTqeWO+Wpus0Xb7X0kLpR/Ln0nZSjrl9EmTbhWDBQjMw8XZlTWvTyvL/On0sEgOUrV+25NTjz1/Ka6x3dtobJYhYwd3kxmIwc2ThPjaxJslyZpmaMTRQjd6xYVPjF9m2jg21tijMxUZ9fWHz8idfe3jD16I1TsXGdeTbMDFrmJR5evwKCgEIKBClQhQJV2GATNtiFbcu4pEdbjvw0Z/X7pTGrjhk7rmnxB6u+H5w/c9fi6taq68FwO8IyDI016FKHZAkJHUVZevwlHswSRMKwUNS6rEOyODA+afwvG9YUr/7ow2duBoPq/S5XqGR5afmUkjd3VbZodW9UtvwNTYYhWcbGRh+T82lzU0uvX41FZkUoUEj5dFxyWurJvcsWzn9hCQBMyJt49eP1m9ZfThlaUfhH65/QWYcmNWisQ5cadNZjTbJEUZYWz6mlBIOMXcsSBJqd7hi+dOm66QCQ5nKFvPsOrfqxoeP39060XIvubQNQdkJKlhFCGa9Bq9OtQQKSY4qm2kXGsYqjWQDw7Nznjze2y3O5yWryYXdKMhhwCJJ2AakSeO/V0PnSyuZAzKV7Adj8AoDHZ5y7wrgTN8/PKEixK+sAYOq0py8MTktrosgAjvx4NMMoKCreF8h+/LMXDzYGwCxRPEzGfwGAwQAzJEkQk40opnTkiph9p6G57tzqGTmT+oO5pVcu/71W4jFuOAQi0bRgaO7WDV8XGlbqOZEgMuCmTZ1Wo7oeWDsiSW3vu0W8jRfp0ryMISGdKST5to4JgjhBIR7kELK/StcBdPTpqmWPZVoiMuotxFyY2ffKtH1JrL9aLGAL2AK2gC1gC9gCtoAtYAvYAraALWAL+B6QfwcABV76YJ70DhAAAAAASUVORK5CYII=",
      hum_lev3 : "YISURBVHja7JltjFRXGcd/zzl3ZmeZWWCBfZPdhQLhNdCKlRapSkXbkkqatI3xJcw2YtTWpKZ+IorR2EY/NLZqsFGS0t0B06SN0WqtcbsG0ogEUstS21DeuzPAzm4pu9ud2ZeZuefxw50FbMGk6ZLMyj3J+XbPc5/fPef8n5crqsr1NAzX2QiBQ+AQOAQOgUPgEDgEDoFD4BA4BA6BQ2DAmwwjIvLhFrT3BAsemPehug+T0ayQSTHyPuCr2ZRUxgITE8AHfE22+B/FbkUBSyojCB7gIZgPPoyCOpQSUNLNLTolgSWVNogEoEYMgulYO6uxJiLRudW2tqSIU8ZP5UoDbfsvZFEcqopSRClpssVNGWDaeyIY8RAxGMzBu+pXN8Tskt93PL1ucGAgcfytI/MAEonEyNKVq07fm9yy59yof3RdZ/+bKA6nDtUSbfMKlQ/c0WMQqULEYsXsv6Nu9XOP/3jrju2/2pDP568okg2NjWM//cX2ncs/v+n5tZ3vHMKpH0AzRlurm0zgSVHp9+FbRAxWzIamWLXrS6+bgF2wcNHwmnW3nQAEIJ8bjh385775fdls7OEtbd/sPNh9bn1T7Vt7e8dGAXDqAYWKC0uXdjctCBEMBoO9e25szv6uPUsndvbRHe07c4s/sdcp4kDiVryHBs+s2rhi0Q/y+bz3/K5nPvuth7Z17e0bPx7ImUZIpYskW7UygQWLEYMRixF7dsz5i2fNHlmwcNGwArU3rjn5tddzb2PElM+o61rV2lzf2DjWn83GsufOzZnmmekYsYHyiaLqAcXKBIYIggRiJebnp0Zyv7xl43M7Dmw6PA7eK+/5Z4jaGEYsAjh17/iU6hsaR/qz2Rgg46oWKxYVhyKoRioTuCNty3HWlKEtRux3j+TTRE0fEVNFxMSoMtVY4wGC09KMiDS8cbh7FkB9c8tAtqCjgR3kor1U2pJs9Stthy0iXNzhCYcNFiMe1kSImOj9ddH5n6qx8xUkDvL3nU/dOmHg5g13vrm7v/BuELfFgMplflYQcJAb2wn1Rcqx6tI0GOyt073am7r33LPz0W23A4wMD3unjh2dBvC5uzam7fJPHux8bWiM4LMRfEAVwJTfoZV2h68mZoAw3UhkZPBC9Rv/enX6f8XhpqbxTVsefKV33A1fc1cmLfHoSEexEsVOHF+J4JkqorZ8d211fdyr+VnV+ftOHth3A0Dm9UMNf3t295L+3t6qhqam8ae7/vHE9/pnpo4NFXMUXQHfFSlpCafjJFsLlZVppdIGIRAkKxG84M6WxaqKqI0RMVX11TZ+U42tVZAVUYkvPdm99tvrb/k6wMPf/+GeG76x9SePHH7vGEUt4rsivpZwOkKy1VVWppVsdexKlwsAcag6nPr4WsKopeQKAP2qrnPMHwF42al7YeXH6ybicN+ZTO1tVSaGI1ivKIojeSm9rKw7HFQ6NoDFx6nZvjzevKTGW1RE7MsDxcyTZwt9mHI9rLiSz9BEHBbQKiP+pVxaHWhxMl2cbNHyy7AOEf/Zm2csPPDUY9t++9eXVgM83nVg65OnR3suy7Q0LlqQi+qrGhWCk6EEdoI6uUKBk61KKl3CYRCVuqgx+aGh2L8Pd89uaGwc8/ozzY8taKrrKwTHe3bEeA1REz914vgMgKaPzT1f8HUIv3wdVCc1j742YUkpgVocnB3x3122ctVp4NN92Wzska/c9517vvzVG5tnzswDDA8Oxh988U9r8vm8F4/HS19KPrD3R0dz6cvKw9Lk+6f6kecH08yeCKl0NbsziSNDxTs3fOGOE+Wk4YozHo8Xn/j1b9pfu1D4DLszCVLpaXSko9fC12vY4skYBO+P6+c0rJgZWbbvL3/4YudLL64FyOVy1YlEYhRg8dJlb9997/2vTmuat2/Fn/sOoVoEirq5RadmE29XRgDvhdvn1C1IePMBPCPRMT9Q3/Pj7sLvTo/0tp/M5VD8KdvEu+JzuzKmrBuXt2lLurnF/d+0aa+6piMt2va/FbhigafSCH+mhcAhcAgcAofAIXAIHAKHwCFwCBwCh8DXI/B/BgDMQlD8xJeczAAAAABJRU5ErkJggg==",
      hum_lev4 : "VdSURBVHja7JlbaFxVFIa/tfeZOTOTadQW0lqTkKqpt2CtgqAWrVqwoqB4QW2aKIIvKopP3kBBLaI+6JOoIF6SiDfUotIHH3zwUqxoLa2gqNR2ovVCetEkM3Pm7L18OGfSIF5amsjEngMz83LWZn/733vtf60RVeVwegyH2ZMBZ8AZcAacAWfAGXAGnAFnwBlwBpwBZ8BAMJODicjBBby4M1nwwW5/MGGH0rSQmex4NIH/bUwZGc0D4bQFbwCR9ndGB7KAcwJYRkYFKCIUEAkAQUgD0ORbY6CGUtX+Tp2TwDJcCRAJEYqIWAwWEXPJ4sK8U9pzbQBGVN/9sb5n695GHVWHV4dSRbXGQHc8Z4BlqFLCSAEjAYYAIwEi9oG+ed3H/bDlih3ffLUIQECXr1j5ze0/tg9/va9RwxPjNfmo1hnoHp8p4GC2sqEMV4L9sJLDSg4jOQzBsiNzJ9177a1rt235Yn7z/XVPPfv6ijOvWvj1hPsJpwbBIAgOGKpUGehyLZel/yR3OA02j5U8RnIPnVw+6eMXn7x8OiyAExNMQoiVEMHgaB5gxWkRGG/Ze1hGRgWhiCHApupayZdCWzy5Pbf0+ccfWwnQd9rpe/8EXCAwBawJ00XKIRJgpMhIRVoWGAiTBJUqbCSPNfm7e0t96x++58qff9oVtpXLbtXAjdumgI0JqiKlBFhCjMmnsQFCABRbF1gopdnYNlXuKQfzeiZ/OeP1Z58+FWD1dQPbw/kL4mkK5yIjBQIpYE0+PfNJshOxiLQocHINWUQMgsUkSg92hr3rn3j4wonxcdtWLrsVd9zzeWxtfrrCDSPN7ZzCSnPRDBAwXMm3osKF1FQYjBhEzEUd4YJFO7ees+HVl3oBLuq/fuen8xb+4G0wBeCNDZwxU8ktUZYENlk8ScduIeDEG+eSyYkkVyzm0o58z2uPPbiqqe7Zt9+1eWtM3VmTm7pbjRgvpKo2IWX6OALkpvx3iyWtqYx627HFxdEn7537/oZ3jwFYvWbw+y/KHd8BooL8ZZz804itpHBS9TQSb6wK6Hnz8z3Prbt/NUBbuezOv+3OjRv2uopxPjKxb0y3TwZiVB2KR9UnlkpTn40C0cFWVv+F8agBJRR/y5LSgq/Wj6zctmXzUQAXX9P/7REdR//2CJwaiYTfeTdVHRnn48vb7dGLNZh4Y1d9H0oK3oRHEWqHvPVmw0szUpmPNYWhs45a/s59t6x75YXnlh1IfN/yM/bd/PZHj9785fgHNHyV2FeJfR2nNZxWWdM5dqheerbOcBVVHxrcgc5uSe/Sie7je8d2R343zkd4bSTFA3FSPenkjIgyKwoPVwRhwYqFhfY7Tiwv6yrZE+pKEClBQzWIVAIvmM/eernvwZsGLwa4/5kX3imsuvrtN3fVvt001tiN0zrOR8mvRqj+Sn+XtmS1pP2dKiOjtQ9/qdsPx6JPsbIluV9NUkgYgssWhR3LvT+hGWPVu817Gjs2jTXGcBqlsKnKOtmEbd1qSbWKJ5xW8yjWe1QcIkEZJq36KWtp1MdtQo3Y1/Aa4zTaXxNTbfmupa7tcqjWcBrjtIHTiFjrxL5O7GttQt2quv0Ka1wW6ukWrqdnuIGnxtqZqYVnV+EEekKGKzU8RaCQtG8kxqit/B7vvqB36ei1gzdsBjiud+noxvF4D7Gvo8SoVlEmGejyMzmn/7aJJ4QIbSABglzdVSxfv6TUYwUd2l7d/tKOyX1pNq7pmjnaxPubBkEubdPmU9MYIdR0TWfjf9Om/cv3hyoCiP7Ntm154LnwZH+mZcAZcAacAWfAGXAGnAFnwBlwBpwBZ8CHI/AfAwA6fonTPJsvwAAAAABJRU5ErkJggg==",
      hum_lev5 : "YPSURBVHja7NlbjBvVGQfw/3fOzHiNHWev9sZ7CRtEQkJCQOIWECGXqqgUh6ipFAla9bmV6ANIVFXFCwSp4lrUSi0PVR+KWqm0kGRBRGpF2pI26TW9JNlNsoSw2cS3vdnr68yc7+uDdzcLAgGqg7zKnNFoZHnO5/PzmXPO52MSEVxNReEqKwE4AAfgAByAA3AADsABOAAH4AAcgANwM8FEtHh+qvv3Z6OfNXZT2tmsHY+lDfq4mDSc0wB6ACQAhAD4AKYAZCQVr39S7Ga09fMA0ysTtd7XM/VVaVd6BFAMUQARACFAFMArLSq8XzHnT23rTC9b8DeOFwbv6nTubrfVah/QPqCNQAlA0hALAGiAFcAVw6WLVb6w70z5GERysivhNhtsXYmJgfZnwlC0bt/66OahiL7xxKH9dwqgBERy+ZtZuAqERYFkw+1bMn64+1h7m8rP1jhJB7LvyoOJqWa2zboyUyEloCgcc1RHzFarHvv63ns+TbVnXvnV0fZtqZXXRa2Of3geAxiYH+MtDD6Q1VDogVahusD25PJn9Cb7vHiy72Mnp0hHl1cV0kUhDU0hCITeyMUAFFsXrNAJRW1Q5HiAbQC98Nb23Xuy9z/5/N8WxvD8cy0KYA34OZenTpb80tk6CxQ50GAYJFsbDCShyIYm2wN9oIeFSOUNzJGyyQmRIkAIwkrgaxZv0uXC76a9dANLDIEBSxzD2feQStRbD3wwG4OiGBRZULB9wGJaktwopTyCYzSFuywVBoBpX8pFn6t5D+V/lbgErRwIGwgZKHhQZEEQB3Ch9cCEHhBpEDSItGksQ4uPtADkE9lfWKGvi2lqA4CikarPVEu7Mr0uRJE/znrjaUM+iDyQWCBoAH2tB96faWRRBNVAk2ZACS0uPxi86Zbyv/d957ZTx/4cPfGXo20AsOmOLbXuvv76l7757fGBzXeOxQj4peeeLhnUQKQap9g4mO0GMNlKPdw1v7bSwiFEi5MTAPzmhe/3nxs55Syt9N8GvO3wa69uevzln4XW7nq4fG/M6n6zziWQKNB8zEY6Ovn/z6lNzzo++HIpODtxwXr0pz8ff/b46JmflOTUS+P509977c3za9ZvcAHg1y8+szrp0Mpeh8IfikkATKuN4SkAayCXj7zHlZJR5Y233l4mIrpn70OTHbsfGhsx8CoVJqutE/1b77Mefoq9p76auv7c6EgoqmB3ajiaACPg+VgMINta4N29BsPZPAQDEGGIcLrOpbQrM48f+tNvPYI94cM9U2P3YEVmCoAkNTl7I5RwRHjxEX9ruLd765fH1rYpZ6RuGA10DbsSM2jBXDoDkSSEDATmRMlM94ZUeqUntYhF0VFXan+oYq4gAAhKgXxLVF0tATcSEWESaazDIqZZvdv8MZxKlCGYA4sPFi/VafVsj1n9X4zpa7v/c/TGHRHVsy2MCDyuwOOK8rhis9RVA9VokIixAF8xPLD4zQY3P9MSuQhGDCxWn012n0OxnR12CgCGZ7zJpAJttqFCGlYIYEdQV3wZvPbWO3IjvpTer5giWDwwcnggXm9dcCqRx3B2CEa0Bfh2Y1cDAHDirYPx9TtS+SGH2hSgbSIV1XAOvXO4EwAi0ahp70kUzKxXmXNNFSwuRC62/s9DRgYkA66REotUE6uSbjZ9yfnBY4/ctHP37+PxgcFyqVBwKsWC/d7oydjRw2+3A8CW7TunSj5PT9d5FiwuWEryQLxILQ8WycBI13SdZwqeZL72rUdGnn/iu5uz6UvOL378w8GPqpJYlXQffe5H72RczpwvmxyMlMEy3vQ04Upu8ez5e6F/TzK0o9tRGwtnT657+/VXr58rFkLnTo+0A8CaGzYUICI337310pbUV0anXMn/dcY7/vTZ8hEwcrIrbpbjJl5kosYbx8rmhgrLEAtCYU3XAEDFSJUAKfoy57KcGwrrd+/tsv/54axquYEXyorzFdN1ZMbrnKjy6jlfwrM+zw2G9cymFdbY/XEni/lNveW+TfvR9x/IhuXBRPWzxG4p8HIpwX9LATgAB+AAHIADcAAOwAE4AAfgAByAr0bw/wYAhJQKqkItTDYAAAAASUVORK5CYII=",
      hum_lev6 : "YxSURBVHja7NltbFvVGQfw/3POvbZjxy/Bdpo21HHSvDRpQihUCFYx0dGUCqppkybtCx/2YeNFQrwIiRfxBSGBtn0YEyAVpg0QDKFtSJUoNCUVSIgJtTDoQlPIQpvmvU1sx3YcO76+956HD46zRhswaS5y6D3S+eRzH9/fPfLR/z4mZsblNAQus+GAHbADdsAO2AE7YAfsgB2wA3bADtgBVxNMRCCi/23tYFKnwWTgUtT+1lrV6nhUbuib6tFQKgigHUAMAAHIgnAGjDneFzb/n9o1BaZjqTiIOqVApMOr1YNADBAxGGCVNjk3b6hxgE/z3nB+Q4LpnaQXRO0QtO3aoNbUH9Aawzr5Qxr5UF5LAJjAbNgwposqs2DYmbcWSsNgjPO+8NSGAdPRZAcE9UKSZ2dAi/y40dXZ7BbhTS7RMDx0eEvZCgAMArCptT1X37b9/LyhUsez1sRfzhvnYKscGMd4f8SsJlir9ilIR5M6BHVBUt3VQS16IOrqbHKLyFsP3bXnzVdfin7ddb27rss/+tqhE3FvQzHuk5mJPADFMQBnq3l/VQdD0BZIqockT59fa9rsFtEjD9110+FVbN9116/03LB7ubJliekp93uH3giO/OMj37svv9Dee89jc81e2TBRVCsAujcAGF0Q5Pbqsi7iFiFfLrW5gt3z059lf/KH10+lFSonMu8W0BOzM7tOfXS8Lr+cc0PAo0mqgyQPGCE6lmoGMFub4KFkFEQRCHI1eYTfp1H9+ImPI5WPDzz2xOyoieLxolpSIIpK6Lf6RBRE1NbdUwrHWoxlBUorCAhyQbAJprbaBRNaIUiHID3ionqvJO/YqZNrASO4rdNMrbAM6SJQJyBdRDRpw/rFkQ8+cwP2hZLKnjVUccaCqtSB4hYcS36KgUi+tsDvJHWAWkGkgaDpklyCSC8s5/TKEptIi7so2C/hcRNJHYyCQqnIbF4weWXGRPHjvMou2szlOqSBSALcCeBkre1wDARRniQZJLgcXdcyYSGdco8/97vY6IcfeBamp2R+KUutPb2mLxBUV++/Ldtz+51jDUKDYBhjOatYqQVQvPbAjDYQaBVNTBBMWBeAnz3wo/j46ZF13zly/EMXAJwYGvTcduqf7ht/c/CTHg8Fx5aRApEAMQFw4WgiBmCq1k5pWv8M8B+J/1fPHEwH49sUUzlaGukUv/3bJ0Pjp0e0t1/6Y2jv3Q80XtnUngxJ0jLWxUGDzNraYcI4GFEwGAATgwVDrcP++Y2ZuVjHUoJJoAxWAbB9R9/O3CPXbG8FgNnhT/yBLe36Jp30jMG8Wi+P/ZHzqELSqub78BQYdnmyTWBFgCpvdHk0tHYU5kq8PJhX84N5deFIXl2YNTnf0NZRqKyZHhn2BSW0qEYaGArMCsBE7TUAbomYAM6B2YaCTQqWAKuLw7pkWIJRWiip7IKhlhYMlZPMJcls/ft3wCyYbGK2odgCwwZwrjY7HsxjYDbBbEqwKRnWxYlfgi1NKQMWF2CXp1RsSMY6sAa2JMMEwwTzJAbC+doE74tkoJCEYjNpcL5gc75lx1XZtQODYWoKxS1ga3WammJDw1rUBDErCViC2YRiE4zR2s7SzKOwEV4yVc5WWNnU0pqrfJQ5M+rqjnV6ooKuYCIiZvYQUSmVWHvwO3b/MJGzeGmuqLKwOc17r1igWgbzQHiShlL90wWLZopacmDXD6baurbvGP/XqOfRm2/o//m9D57f2rdz7SEUMovy9396vhkAfH6/6hs4MPl+2kp/uWwtQvEX1b6/6u8wACgegYWdi4ZKJwyVuO/pg8NP/fL2a+bnZvUXn3y8+b9d4vP71b2/fvrztMkLmZJK2xanwDxZ9ff1S9fiSXkhEL+/pW7fDr/sjuoifOKvr3RNnP4sBCI69/lIfWPz1qIvEDAbt8by/TcNzMn49jMJQ316JFk6/O61gdmN2sSTB2eMPWlLdfs1avdJqmOAfJI8hoJpM1vMKER0+qLDK0/2+OQwAGNDdy1Xh/63+dLmv2fM3umiag5pVA9gscsnzz4crzsDIAtg5XvRpv2a3leA90eWLkXt7wS8UYbz35IDdsAO2AE7YAfsgB2wA3bADtgBO+DLEfzVAMDRDt/FdBbSAAAAAElFTkSuQmCC",
      hum_lev7 : "XiSURBVHja7NnfbxxXFcDx77kz+8s/4vWuEzfUiZsmVErbJEqhgYACqhInJfwQAgEiQNunvqFK8AZ/AP8Az4AolSIhURChVd1ICIL6y2ooNTGtm6SN3SaNvbtZe3c9O7P33sPDbtqolWgfbLRp5q6OdvbhHt3P3KujmbOiqtxKw3CLjRScglNwCk7BKTgFp+AUnIJTcApOwSkYCNcjiYi8d/1xGgryTGUQQI+NtdY790fmW5ckH72oACh87Z+NbRcjd2fLMwGQN7K0LW8unL5v+CKQ9OKmBg8utv2OU5XO7rfa7u6WZ1jBeMQACKoG/ICR5kROzh8qZv6zfziYB1ZvOrBMV0d/PFk4vr1gDgyFMpYzkncQ+F7dUBABFdAAnPUk9Y5fWWz7l365EE3rVHlxI8DhRhQGma4eyofmrhWvuwZCGd8SXdvxxtmZoqKiiHxQYUBv27GzNfypnRc04d7JgaAg09U39Wj5L31ZtD6AHcLIrnLejORC2RQYGXr95ZnSz773jV3/a94PHvvp8rGf/6KSD3W1lAtGL7V9LM9WbwPe7WswRvYTSKEt5CKVbBvCQqmsez53sC29nb3xYF48N5tpNRrGI6YD2Y6Qc4HkCaQA7Olv8HQli7ATI7mqJ6x55IrFb917oPKTp860VTCuW7EFQFZq2cf27boDYGBkRGMI25CJIUtg8qj/NKerL3Ck3OhPsJFJAhkikDzG5GZj34kDs7ZdfW7ACk4ko0KggpkMpXD5z38Yba2uCsAXHnm0vujw15yYZY/BSA4jHVT3AM/165Hei0gWkQyG7KJFlyMfXVKTG8pgVNBsINm9OTOwKWTg5MnfDQF8/tjxuFMs21rkuezU1zwGQxYjWbzsXk+wWcfjXEJkM4YQQwYjGYxk20J43qKvJLgFjymFMjQSMhAvvjk4+9w/QoAvnnhoreLwVY9etuoxkkEkgxAiDHK6elc/PkvfjRAgcj3C3u8QIyFGMpszplDKSKEYSv7FJ34zCDC4aZPu/uZ3omWPXrX4dxwOIcTckAvu6S/w08sZ4E4EAaT3MYiYLrobI6HkBoxki0ayf3vitxmAB77/o6TmcQ3FL3m1CQgiBkTeywdlnqmM9tsOyw3xoTEWEI4ZwhGDqb16NlxaXBCAAz98pF312IrDvmvpfCjf+9mS/gF/ZXMHuIDiAY+iKB7V3je+bERKBsYEmXn811mA8W3bdWzfZ+IVh61YHy9ZjXvzrufp5oIrfIw3q//vDovMXceh6lF1KA7FZlXd1gAZEXTUoDOn/hQA3H/860nTabzmNb5mifFqe3Mcqg7Uozjgjf4rWlPlGqpLqFqUDqrd8JqMCr6I+nFBz//x97mltxcNwMETDzXrTturVqOa1RZeE7x2emHxdFCtc6Q8368dj9n3F02C0xivyRaDHzP4EVGdP/PX7nGe2ObH93222bS0Kh1tXo19A69xLxKUpHvTmOvfFs+R8jyeJl5jnCZ4beM02mxwRcGVDPriU6dyAAce/GrUsroWWb+2kvgGzkc4beO03ZvfzYG+3t89La9zvUVHOI0mAnW3B8qYgaWzL+WX3nk7APjyiYerDaeNhvWrlcSv4HStGz7CadTb6XkOr99z9MaAVc91d6q78NtD0XEjOh4gf3/8V0WAweFhP7n//nrb+mY10fqVyNWwvoXzPbRGOB/hea3vu5Y6VW7idQ6ra1jf2mKwpQBXMiIvTD89BHBw6sFm22kjsbraTHwd6xtYbWK1idPr8Ld0qrTQ9w0AAD1afl6erc7iuC/qaCFxWoyc1qe+/d0qwJ5DD6y0rK83rdaXI1fDarNXmWO8nkN5VafKtQ3pxmx01/LJ5aRslWNe+VJg2KqINJ12qh1tvdx0V09ejRdQ6ij/QvU1PVxKPglt2juAibmW2/r8ip0813Tla1bjC5H795m6ndHDpUuftL40wDAwCPDkUtL51iuNth4t35yN+JtppH+mpeAUnIJTcApOwSk4BafgFJyCU3AKvhXB/x0AbJ9oVmAXjo8AAAAASUVORK5CYII=",
      hum_lev8 : "aDSURBVHja7NlLbFxXGQfw/3fOvXc8M56XHSd2nczYiZTSNh7biZU0sR0nBoSK2k2RWlEEgkosgAVdABVLHgIipKggwkNigYRYoIoFSjCLtK7t2AmGmjh+KG1o4pg8KseJPTP3zuO+zsfCnopKBTXSWBor92zvuWfO75x7Pn3nG2JmPEpN4BFrATgAB+AAHIADcAAOwAE4AAfgAByAA3CtwET0cP1H13fS6HriYcZ/2N/4n2PVouJBRPg44xydtfqZua/oYzcA2MyX8x5PrxxNXP84C1qTuW41+NWl8u4VF0MFnweiEolGTTQwgwDAUuyYHju24pWCx+cvFbxxHkxWtiWYxnKZgZQ23BoSJzINoikiKRzXRDgiYQCbYJ/dioJjelxZslXuXcu/v1D03oTCFA8lV7YCrG1FYKCx9ZdbG+T+nSG55/Go2NUeEsmURpF3x84nZ6bGo9V+j/cPldLdB4tOvNlq0CisAC3P+NStktdLY+vTfCJ1rtZz07YAux+COvY3ytZ0WOx4LCSabp57vf3br3ytpWgWPhx5Tp+KA8A3Xvvl+hMvffWupYCCz3zLFg6gjtBEbpKPJ3N1DYagfl0TjVFdxGM6JUJWLnlmE7v3qS6/75lnnWrXqxcv6PMXJ7Uzr3w99ePsQTv5xEEvYUi7pUFZqxUo+OoYgJG6BdNELglBn0iGRDyiiaghKLw8989Y0SxQNB7nVydn1+4pKN48w/0C4mcDPakbi/NyduTP8b6uQ2ZYo2hcl7FVlytgerrW4NomHkT9kBRK6KIxolEkLKnh2tR4BAD2dnUrC5DvuExvO4xFj6kCaC2Zjo2ARCQ8kKEIBks0QFADBCVpMn+ojsHoA1GIBemKoPsErbqbDEARaUpQ6JqCpohCPpFeDcFMICZIX5DBRAYEhSDIAGGwLsE0kesBUQyCdFOxsBSoyKD2bK8PAAtTF2RYQI9I0rMhESZBOhG0Yj63EchIkCJIBjQW0CFIx8aCpDGR66zHHe4BQYIgTQZZDCox0Nbd61U7/HvyLX2fTnq3QUa3QYY083Jp/goBwGNd3arIG4tkKaLqWAAkwL31FbRG13YC6ABBABBlBgoK/MCH2rVnr3fks8/50yNn5ZnPP691Zns+eO3e8k0U83l0Hshy5tnnnas2s8ng8kaCIUAQG1kNejC69hcAdl0mHrs00ts06DskfWj8Yj6PhQvjH30kCjnRGkloBY309zXS33M2oiDABKBSX5/0cNM9AMvgjdjUrpPRqpPeIiHHv/vN+PTIWdl5IMvfGXnT+8Hikv+ayer7i0vqW38dVSe/8CW1tDBHP3/mRGSHJBEjUItGEgCDWQGsQHgHw012vZ3hy2D4YPgJQYgR0CIhz/3mFwYAvPCT07Y4etK+05Zx51x277ZlXOfpE87nfvU7BwCWFudFfm5Ga5YQCQHEBaE6HkCX6i5o8fHkLJjNZglKCEZcEG6Mv2FUn3cMnnTWPHau2Fy+UuHKrM2Vks8uMfxqn+sTb+kJAY4TkCJmMLtg/AvHk+/Xa8XjAil2JMOVzN7mJw4AkAxPMFzP58o1hy3TR9n22XaZnb1PdSkAKOdz0AFlAJ4OuFBwwPz3+s2lGTPE/JxgdgTgCeYPdk8CnmR2Woj9uAaSxL5k8gXD2MxLEI0nfCh2WLHt+1yB4hwPJGaohlOs6Q7zYCJnujxT8bno+lxOZ3vN6rObY+dlRoIPG0SHDaLDOon9GljmHvCNxXkJAOkD3WVHoWL7XLJcZULx3+r+eljxeKLo8rGip0wt0RzrOnLMnp++GDr9xReahl98Kbynq8dtznR4pXxOXpub1cf/+IcwAERjcU5newu3fC5VPGXmbVUA81Tdg3kouTxw2VqseJy0PBX9yg9/eufUl1/MrNy5Lc/+9teRj3onGovzy9/70aoTa1o3S36+6HLB8fkSD9X2LryVJR799yvOsM34NIj6ItZa8/zY+dTywlyEASoV8jIST/gA0JLOOE8OfjKvpffdv++oVdPjMY/59VOd4dvbrYiXAtC55vG+ty3/4G2bDzCQVGAhiIRiKIDhMjwJrLQb4h+HGuWlNoNuALi7XauWUQC7ATQCiI7lvc6pgvfkAwdpD8x7QuJ6NiqvfialvQfAAnAbQGFbl2n/K1ZEAYQB4E+rTrLNEOVjCc0G4AAo/r+LQd2Bt1ML/lsKwAE4AAfgAByAA3AADsABOAAH4AD8KIL/MwAM+CWyYqDvjgAAAABJRU5ErkJggg=="
    };
    window.nemesis.dashboard.render.PortalMarker.getGlowIcon_ = function(a, b, c) {
      var d = a.spriteNameString, e = nemesis.dashboard.render.PortalMarker.glowIcons_;
      e[d] || (e[d] = {});
      e[d][b] || (e[d][b] = {});
      e[d][b][c] || (e[d][b][c] = nemesis.dashboard.render.PortalMarker.createIcon_("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAEZ0FNQQAAsY58+1GTAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAA" + window.nemesis.ottaky_base64_icons[a.spriteNameString + "_lev" + b], 60, c));
      return e[d][b][c]
    };
    
    // highlight player mods
      
    window.nemesis.dashboard.soy.portalinfooverlay.mod_ = function(a, b) {
      var c = b || new soy.StringBuilder;
      c.append('<div class="mod">');
        null != a ? (c.append('<div class="mod_installer"><span title="', soy.$$escapeHtmlAttribute(a.installer) + '"' + (a.installer == PLAYER.nickname ? ' style="background-color: #606060; font-weight: bold"' : ''), '">', soy.$$escapeHtml(a.installer), '</span></div><div class="mod_icon"><div class="SPRITE_', soy.$$escapeHtmlAttribute(a.team.modIconSprite), '"></div></div>'), nemesis.dashboard.soy.portalinfooverlay.modName_(soy.$$augmentData(a.stats, {name:a.name, rarity:a.rarity}), c), nemesis.dashboard.soy.portalinfooverlay.modStats_(soy.$$augmentData(a.stats, {stats:a.stats}), c)) : c.append('<div class="mod_icon_empty"></div>');
      c.append("</div>");
      return b ? "" : c.toString();
    };      
      
    // add resonator % and highlight R8s
        
    window.nemesis.ottaky_max_levels = [0,1000,1500,2000,2500,3000,4000,5000,6000];
        
    window.nemesis.dashboard.soy.portalinfooverlay.resonatorLevel_ = function(a, b) {
      var c = b || new soy.StringBuilder;
      if (a.isRight) {
          c.append('<div class="resonator_level">', a.level ? Math.round((100 * (a.energy / window.nemesis.ottaky_max_levels[a.level]))) + "% " + (a.level == 8 ? '<span style="background-color: #606060; font-weight: bold">' : '') + "R" + soy.$$escapeHtml(a.level) +  (a.level == 8 ? '</span>' : ''): "", "</div>");
      }
      else {
        c.append('<div class="resonator_level">', a.level ? (a.level == 8 ? '<span style="background-color: #606060; font-weight: bold">' : '') + "R" + soy.$$escapeHtml(a.level)  +  (a.level == 8 ? '</span>' : '') + " " + Math.round((100 * (a.energy / window.nemesis.ottaky_max_levels[a.level]))) + "%": "", "</div>");
      }                
      return b ? "" : c.toString()
    };
    
    // highlight player resonators
      
    window.nemesis.dashboard.soy.portalinfooverlay.resonatorOwner_ = function(a, b) {
      var c = b || new soy.StringBuilder;
      c.append('<div class="resonator_owner">', a.isDeployed ? '<span title="' + soy.$$escapeHtmlAttribute(a.owner) + '"' + (a.owner == PLAYER.nickname ? ' style="background-color: #606060; font-weight: bold"' : '') + '>' + soy.$$escapeHtml(a.owner) + "</span>" : "", "</div>");
      return b ? "" : c.toString()
    };
  }
  else {
    setTimeout(myscript, 1000);
    console.log("waiting 1 second");
  }
}

inject(myscript);

