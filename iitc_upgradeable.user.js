// ==UserScript==
// @id             iitc-plugin-upgradeable@ottaky
// @name           IITC plugin: Upgradeable
// @version        0.1
// @description    Something something
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {

    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.upgradeable = function() {};
    
    window.plugin.upgradeable.portal_added = function(data) {
        
        var our_portal = data.portal.options.details;
        
        if (our_portal.controllingTeam.team == "ALIENS") {
            
            var deployed_resonators = [];
            var already_deployed = false;
            
            for (var our_resonator = 0; our_resonator < our_portal.resonatorArray.resonators.length; our_resonator++) {
                
                if (!(our_portal.resonatorArray.resonators[our_resonator])) {
                    
                    // empty slot
                    
                    deployed_resonators.push(0);
                    continue;
                }
                
                if (our_portal.resonatorArray.resonators[our_resonator].level == 8 &&
                    localStorage[our_portal.resonatorArray.resonators[our_resonator].ownerGuid] == window.PLAYER.nickname) {
                    
                    // this resonator is a L8 owned by the user .. can't add another, so next portal please!
                    
                    already_deployed = true;
                    break;
                }
                
                deployed_resonators.push(our_portal.resonatorArray.resonators[our_resonator].level);
            }
            
            if (!(already_deployed)) {
                
                // our player hasn't deployed a L8 on this portal
                
                deployed_resonators.sort();
                
                for (var try_slot = 0; try_slot < 8; try_slot++) {
                    
                    // shallow copy of deployed resonators
                    
                    var upgrade_to = deployed_resonators.slice(0);
                    
                    // you can't deploy an L8 over a pre-existing L8
                    
                    if (upgrade_to[try_slot] == 8) {
                        
                        continue;
                    }
                    
                    // upgrade resonator to L8
                    
                    upgrade_to[try_slot] = 8;
                    
                    // bit of a braindead check for new level (should really use hardcoded strings)
                    
                    var new_level = 0;
                    
                    for (var get_slot = 0; get_slot < 8; get_slot++) {
                        
                        new_level += upgrade_to[get_slot];
                    }
                    
                    new_level = parseInt(new_level / 8);
                    
                    if (new_level >= 7 && (new_level > window.getPortalLevel(our_portal))) {
                        
                        // adding a L8 to this portal will make a L7 or better portal and bump up the portal level (i.e., not just putting another L8 on an already L7 portal)
                        
                        var our_portal_guid = data.portal.options.guid;
                        
                        var levelMarker = L.marker(data.portal.getLatLng(), { "icon" : L.icon( { "iconUrl" : 'http://ottaky.com/ingress/images/' + new_level + '.gif',
                                                                                                 "iconSize" : [45, 45] } ),
                                                                              "portalGUID" : our_portal_guid } );
                        
                        levelMarker.on('click', function() { window.renderPortalDetails(our_portal_guid); } );
                        
                        levelMarker.addTo(map);
                        
                        break;
                    }						
                }
            }
        }
    }
    
    var setup =  function() {
        
        window.addHook('portalAdded', window.plugin.upgradeable.portal_added);
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
        
    } else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);
