// ==UserScript==
// @name           IITC plugin: Jarvis
// @version        1
// @description    Jarvis responder
// @include        http://www.ingress.com/intel*
// @include        https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.jarvis = function() {};
    
    // unix time of when we started (so that we can ignore old enquiries)

    window.plugin.jarvis.started = 0;

    // message IDs that we've already serviced (so that we don't respond to them twice)

    window.plugin.jarvis.handled = [];
    
    window.plugin.jarvis.handle_incoming_plext = function(data) {
        
        // go through all the plexts in this batch

        for (var this_plext = 0; this_plext < data.raw.result.length; this_plext++) {
            
            // this is the unique ID for this plext
                        
            var plext_id = data.raw.result[this_plext][0];

            if (data.raw.result[this_plext][1] < window.plugin.jarvis.started) {
	
				// skip requests made before we started                

                console.log("JARVIS: skipping old request ID " + plext_id + " timed " + data.raw.result[this_plext][1]);
                
				return;
            }
            
            if (window.plugin.jarvis.handled[plext_id]) {
                
                // we've already dealt with this plext
                
                console.log("JARVIS: already dealt with request ID " + plext_id);

                return;
            }
            
            var plext_content = data.raw.result[this_plext][2].plext.markup;
            
            if (plext_content[0][0] == "SECURE") {
                
                // message from another player
                // 0 = secure
                // 1 = sender
                
                //if (plext_content[1][1].plain.replace(": ", "") == "ottaky" || plext_content[1][1].plain.replace(": ", "") == "LeeLang") {

                if (true) {
                
                    // go through all the parts of this plext (array varies in length .. depends on the number of @....s)
                        
                    for (var plext_part = 0; plext_part < plext_content.length; plext_part++) {
                    
                        if (plext_content[plext_part][0] == "TEXT") {

                            // this is a player sent plext. run our tests against this part
                        
                            // look for a message like "something whereis playername something"

                            var whereis_match = plext_content[plext_part][1].plain.match(/whereis ([^ ]+)/);

                            if (whereis_match) {

                                // we found a whereis request
                        
                                console.log("JARVIS: whereis " + whereis_match[1]);
                                
                                //window.plugin.jarvis.send_spam(window.plugin.jarvis.whereis(whereis_match[1], plext_content[1][1].plain.replace(": ", "")));

                                // we handled this text (as far as we know)

                                window.plugin.jarvis.handled[plext_id] = true;
                            }
                            
                            // look for a message like "something whois playername something"

                            var whois_match = plext_content[plext_part][1].plain.match(/whois ([^ ]+)/);
                                
                            if (whois_match) {
                                    
                                // we found a whois request
                                                                    
                                console.log("JARVIS: whois " + whois_match[1]);
                                    
                                //window.plugin.jarvis.send_spam(window.plugin.jarvis.whois(whois_match[1], plext_content[1][1].plain.replace(": ", "")));

                                // we handled this text (as far as we know)

                                window.plugin.jarvis.handled[plext_id] = true;
                            }

                            // l7nw, l7se 

                            
                        }
                    }
                }
            }
        }
    }
    
    window.plugin.jarvis.send_spam = function(spam) {
        
        console.log("JARVIS: sending " + spam);

        window.postAjax('sendPlext', { "message"     : spam,
                                       "latE6"       : 51485552,
                                       "lngE6"       : -42744,
                                       "factionOnly" : true },
                        
                        function(response) {
                            
                            if(response.error) {
                                
                                console.log("Spamming failed");
                            }
                            else {
                                
                                console.log("Spammed OK");
                            }
                        },

                        function() {
                        
                            console.log("Spamming failed");
                        } );
        
    }
    
    window.plugin.jarvis.whois = function(player_name, request_from) {
        
        // return some useful information on a particular player

        var lc_player_name = player_name.toLowerCase();
        
        var return_message = "";
        
        var player_guid = false;
        
        // try to resolve player guid from player name (case insensitive)

        Object.keys(localStorage).forEach(function (this_guid) {

            if (localStorage[this_guid].toLowerCase() == lc_player_name) {

                // we got a hit

                player_guid = this_guid;
                player_name = localStorage[this_guid];
            }
        });
        
        if (player_guid) {

            // we have a player guid, we can look for this player on the portals

            var owned_portals = 0;
            var deployed_portals = 0;
            var deployed_resonators = 0;
            var min_level = 0;
            var min_portal_level = 10;
            var faction = false;

            // go through the portals that we know about

            Object.keys(window.portals).forEach(function (this_portal) {

                // shortcut to the portal details

                var details = window.portals[this_portal].options.details;

                if (details.captured) {

                    // this portal belongs to .. someone (it's not neutral)

                    if (details.captured.capturingPlayerId == player_guid) {

                        // this portal was captured by the player we're looking for

                        owned_portals++;

                        // and now we know what faction the player is

                        faction = details.controllingTeam.team;
                    }

                    if (window.portals[this_portal].options.level < min_portal_level) {

                        // keep a track of the lowest level portal we looked at

                        min_portal_level = window.portals[this_portal].options.level;
                    }

                    var deployed_on_this_portal = false;
                    var resonators = details.resonatorArray.resonators;

                    // let's have a look at the resonators on this portal

                    for (var reso = 0; reso < resonators.length; reso++) {

                        if (resonators[reso]) {

                            // there a resonator in this slot

                            if (resonators[reso].ownerGuid == player_guid) {

                                // and it belongs to our target

                                deployed_resonators++;
                                deployed_on_this_portal = true;

                                if (resonators[reso].level > min_level) {

                                    // and now we know that their level must be at least equal to this resonator level

                                    min_level = resonators[reso].level;
                                }
                            }
                        }
                    }

                    if (deployed_on_this_portal) {

                        // our target had at least one resonator on this portal

                        deployed_portals++;

                        // and he must therefore be from the same team as whoever captured the portal

                        faction = details.controllingTeam.team;
                    }
                }
            });

            if (min_level == 0) {

                // L1 is as low as you can go

                min_level = 1;
            }

            if (faction) {

                // translate the faction into the names we know and love

                faction = (faction == "ALIENS" ? "Enlightened" : "Resistance") + " player."
            }
            else {

                faction = "player."
            }

            min_portal_level = parseInt(min_portal_level);

            // and now we can return some useful information

            return_message = "@" + request_from + " - JARVIS says of the " + Object.keys(window.portals).length + " portals >= L" + min_portal_level + " that he can see, " +
                             "@" + player_name + " owns " + owned_portals + ", " +
                             "has " + deployed_resonators + " resonators on " + deployed_portals + " portals and " +
                             "is " + (min_level < 8 ? "at least " : "") + "a L" + min_level + " " + faction;
        }
        else {

            // we don't know the player's guid so we can't look for them

            return_message = "@" + request_from + " - JARVIS claims not to know anything about a player called " + player_name;
        }
        
        // done
                
        return return_message;
    }
    
    window.plugin.jarvis.whereis = function(player_name, request_from) {
        
        // return the last known location and time for a given player name

        var lc_player_name = player_name.toLowerCase();

        var return_message = "";
        
        if (lc_player_name == "jarvis") {

            // easter egg
            
            return_message = "@" + request_from + " - JARVIS operates from a secret location somewhere within the M25. Thanks for asking.";
        }
        else if (lc_player_name == "ottaky") {

            // easter egg
            
            return_message = "@" + request_from + " - JARVIS says @ottaky is outside attempting to open the pod bay doors.";
        }
        else {
            
            // in case we can't find a record for this player
                        
            return_message = "@" + request_from + " - JARVIS says he doesn't know where " + player_name + " is.";

            if (lc_player_name == "fizzlah") {

                // easter egg
                
             	return_message += " He's probably still in bed.";   
            }
            
            // go through the stored players from "the other" plugin
                        
            Object.keys(window.plugin.playerTracker.stored).forEach(function (tracked_player_guid) {
            
                if (window.plugin.playerTracker.stored[tracked_player_guid].nick.toLowerCase() == lc_player_name) {
                
                    // this is our target
                                    
                    var now = new Date;
        
                    // make up a response

                    return_message = "@" + request_from + " - " +
                                     "JARVIS says he last saw @" + window.plugin.playerTracker.stored[tracked_player_guid].nick +
                                     " at " + 
                                     window.plugin.playerTracker.stored[tracked_player_guid].events[window.plugin.playerTracker.stored[tracked_player_guid].events.length - 1].name +
                                     " " +
                                     window.plugin.playerTracker.ago(window.plugin.playerTracker.stored[tracked_player_guid].events[window.plugin.playerTracker.stored[tracked_player_guid].events.length - 1].time, now) +
                                     " ago.";
                    
                    if (window.plugin.playerTracker.stored[tracked_player_guid].events.length > 1) {

                        // a little extra information if we have more than one record for this player
                     
                        return_message += " First sighting " +
                                          window.plugin.playerTracker.ago(window.plugin.playerTracker.stored[tracked_player_guid].events[0].time, now) + " ago. " +
                                          window.plugin.playerTracker.stored[tracked_player_guid].events.length + " total sightings in the last " +
                                          window.plugin.playerTracker.ago(window.plugin.jarvis.started, now) + ".";
                    }
                }
            });
        }
        
        return return_message;
    }
    
    window.plugin.jarvis.request_chat = function() {

        // refresh the chat every 60 seconds
        //
        // there's probably a better way to do this
        
        console.log("JARVIS: refreshing chat");
        
	    window.chat.request();
        
        setTimeout(window.plugin.jarvis.request_chat, 60000);
    }
    
    var setup = function() {

        if (Object.keys(window.plugin.playerTracker.stored).length > 1) {

            // playerTracker has loaded and found something

            // keep a record of when we started so that we can ignore requests made previous to this
            
            window.plugin.jarvis.started = new Date().getTime();

            console.log("JARVIS: started at " + window.plugin.jarvis.started);

            // hook into faction chat so that we can monitor requests
            
            window.addHook('factionChatDataAvailable', window.plugin.jarvis.handle_incoming_plext);
            
            // override the default idle time so that we keep refreshing for up to 480 minutes
                        
            window.MAX_IDLE_TIME = 480;

            // request some chat
            
            window.plugin.jarvis.request_chat();
        }
        else {

            // no playerTracker data yet, wait a second and try again (we may go round this loop a couple of times)
            
            console.log("JARVIS: waiting 1s to start");

            setTimeout(setup, 1000);
        }
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

