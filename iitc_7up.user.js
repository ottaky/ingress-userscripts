// ==UserScript==
// @id             iitc-plugin-seven-up@ottaky
// @name           IITC plugin: 7 Up
// @version        1
// @description    ottaky's 7 Up List
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.sevenup = function() {};
    
    window.plugin.sevenup.get_list = function() {   
        
        // the list of portals that can be upgraded
        
        var upgradeable_portals = [];
        
        Object.keys(window.portals).forEach(function (this_portal) {
            
            var our_portal = window.portals[this_portal].options.details;
            
            if (our_portal.controllingTeam.team == "ALIENS") {
                
                // this is an enlightened portal
                
                var already_deployed = false;
                var deployed_resonators = [];
                
                for (var our_resonator = 0; our_resonator < our_portal.resonatorArray.resonators.length; our_resonator++) {
                    
                    if (!(our_portal.resonatorArray.resonators[our_resonator])) {
                        
                        // null object is an empty resonator slot now
                        
                        deployed_resonators.push(0);
                        continue;
                    }
                    
                    if (our_portal.resonatorArray.resonators[our_resonator].level == 8 &&
                        localStorage[our_portal.resonatorArray.resonators[our_resonator].ownerGuid] == window.PLAYER.nickname) {
                        
                        // this resonator is a L8 owned by the user .. can't add another, so next portal please!
                        
                        already_deployed = true;
                        break;
                    }
                    
                    deployed_resonators.push(our_portal.resonatorArray.resonators[our_resonator].level);
                }
                
                if (already_deployed == false) {
                    
                    // player hasn't put a L8 on this portal
                    
                    deployed_resonators.sort();

                    for (var try_slot = 0; try_slot < 8; try_slot++) {
                        
                        // shallow copy of deployed resonators
                        
                        var upgrade_to = deployed_resonators.slice(0);

                        // you can't deploy an L8 over a pre-existing L8
                        
                        if (upgrade_to[try_slot] == 8) {
                            
                            continue;
                        }
                        
						// upgrade resonator to L8
                        
                        upgrade_to[try_slot] = 8;
                        
                        // bit of a braindead check for new level (should really use hardcoded strings)
                        
                        var new_level = 0;
                        
                        for (var get_slot = 0; get_slot < 8; get_slot++) {
                            
                            new_level += upgrade_to[get_slot];
                        }
                        
                        new_level = parseInt(new_level / 8);
                        
                        if (new_level >= 7 && (new_level > window.portals[this_portal].options.level)) {
                            
                            // adding a L8 to this portal will make a L7 or better portal and bump up the portal level (i.e., not just putting another L8 on an already L7 portal)
                            
                            deployed_resonators.reverse();
                            
                            upgrade_to.sort();
                            upgrade_to.reverse();
                            
							// add this portal to our list

                            upgradeable_portals.push( { "guid"       : this_portal,
                                                        "name"       : our_portal.portalV2.descriptiveText.TITLE,
                                                        "address"    : our_portal.portalV2.descriptiveText.ADDRESS,
                                                        "latE6"      : our_portal.locationE6.latE6,
                                                        "lngE6"      : our_portal.locationE6.lngE6,
                                                        "resonators" : '[' + deployed_resonators.join("") + ']',
                                                        "upgrade_to" : '[' + upgrade_to.join("") + ']',
                                                        "old_level"  : parseInt(window.portals[this_portal].options.level),
                                                        "new_level"  : new_level } );
                            
                            // no need to check any more permutations
                            
                            break;
                        }						
                    }
                }
            }
        });
        
        var html = '';
        
        if (upgradeable_portals.length >= 1) {

			html += '<table><tr><th>Current</th><th>Upgrade</th><th>Name</th><th>Address</th></tr>';

			// sort our list on the portal name A->Z
            
            upgradeable_portals.sort(function(a, b) { var A = a.name.toLowerCase(); var B = b.name.toLowerCase(); if (A > B) { return 1; } if (A < B) { return -1; } return 0; });
            
            for (var this_portal = 0; this_portal < upgradeable_portals.length; this_portal++) {
                
                html += '<tr>' +
                        '<td>' + upgradeable_portals[this_portal].old_level + '&nbsp;' + upgradeable_portals[this_portal].resonators + '</td>' +
                        '<td>' + upgradeable_portals[this_portal].new_level + '&nbsp;' + upgradeable_portals[this_portal].upgrade_to + '</td>' +
                        '<td><a title="Click to show details. Double click to show details and zoom" class="help" style="color: #00CC99;" ' +
                        'onclick="window.renderPortalDetails(\'' + upgradeable_portals[this_portal].guid + '\');return false" ' + 
                        'ondblclick="window.zoomToAndShowPortal(\'' + upgradeable_portals[this_portal].guid + '\', [' + (upgradeable_portals[this_portal].latE6 / 1000000) +',' + (upgradeable_portals[this_portal].lngE6 / 1000000) + ']);return false">' +
                         upgradeable_portals[this_portal].name + '</a></td>' +
                        '<td>' + upgradeable_portals[this_portal].address + '</td>' + 
                        '</tr>';
                
            }
            
            html += '</table>';
        }
        else {
            
            html = 'No upgradeable portals found';
        }
        
        alert('<div id="sevenup">' + html + '</div>', true, function() {$(".ui-dialog").removeClass('ui-dialog-sevenup');});
        $(".ui-dialog").addClass('ui-dialog-sevenup');
    }
    
    var setup =  function() {
        
        $('#toolbox').append(' <a onclick="window.plugin.sevenup.get_list();">7&nbsp;Up</a>');
        $('head').append('<style>.ui-dialog-sevenup {position: absolute !important; top: 10px !important; left: 30px !important;max-width:800px !important; width:733px !important;}</style>');
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

