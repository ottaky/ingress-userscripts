// ==UserScript==
// @id             iitc-plugin-muzzle@ottaky
// @name           IITC plugin: Muzzle Directive
// @version        0.1
// @description    Highlights L6, L7 & L8 resistance portals on the map
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.muzzle = function() {};
    
    window.plugin.muzzle.portal_added = function(data) {
        
        var our_portal = data.portal.options.details;
        
        if (our_portal.controllingTeam.team == "RESISTANCE") {
            
            var our_portal_level = window.getPortalLevel(our_portal);
            
            if (our_portal_level >= 6) {
                
                var our_portal_guid = data.portal.options.guid;
                
                var our_level_marker = L.marker(data.portal.getLatLng(), { "icon" : L.icon( { "iconUrl" : 'http://ottaky.com/ingress/images/' + Math.floor(our_portal_level) + '.gif',
                                                                                              "iconSize" : [45, 45] } ),
                                                                           "portalGUID" : our_portal_guid } );
                
                our_level_marker.on('click', function() { window.renderPortalDetails(our_portal_guid); } );
                
                our_level_marker.addTo(map);
            }
        }
    }
    
    var setup =  function() {
        
        window.addHook('portalAdded', window.plugin.muzzle.portal_added);
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);
