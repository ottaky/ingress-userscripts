// ==UserScript==
// @id             iitc-plugin-naughty-and-nice@ottaky
// @name           IITC plugin: Naughty and Nice
// @version        1
// @description    ottaky's Naughty and Nice List
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.nandn = function() {};
    
    window.plugin.nandn.naughtyandnice = function(show_level) {   
        
        var teams = { "ALIENS" : { "PLAYERS" : {}, "PORTALS" : {} }, "RESISTANCE" : { "PLAYERS" : {}, "PORTALS" : {} } };
        var portal_guids = {};
        
        var show_table = false;
        
        Object.keys(window.portals).forEach(function (this_portal) {
            
            var our_portal = window.portals[this_portal].options.details;
            
            portal_guids[our_portal.portalV2.descriptiveText.TITLE] = { "guid" : this_portal, "latE6" : our_portal.locationE6.latE6, "lngE6" : our_portal.locationE6.lngE6 };
            
            for (var our_resonator = 0; our_resonator < our_portal.resonatorArray.resonators.length; our_resonator++) {
                
                if (our_portal.resonatorArray.resonators[our_resonator] && our_portal.resonatorArray.resonators[our_resonator].level >= show_level) {
                    
                    var our_team = our_portal.controllingTeam.team;
                    var our_player = localStorage[our_portal.resonatorArray.resonators[our_resonator].ownerGuid];
                    var our_portal_name = our_portal.portalV2.descriptiveText.TITLE;
                    
                    if (!(our_player in teams[our_team]["PLAYERS"])) {
                        teams[our_team]["PLAYERS"][our_player] = {};
                    }
                    
                    teams[our_team]["PLAYERS"][our_player][our_portal_name] = 1;
                    
                    if (!(our_portal_name in teams[our_team]["PORTALS"])) {
                        teams[our_team]["PORTALS"][our_portal_name] = {};
                    }
                    
                    teams[our_team]["PORTALS"][our_portal_name][our_player] = 1;
                    
                    show_table = true;
                }
            }
        });
        
        var html = '<p>Include portals with resonators equal to or better than: ';
        
        for (var a_level = 1; a_level <= 8; a_level++) {
            
            if (a_level == show_level) {
                
                html += 'L' + a_level + '&nbsp;&nbsp;';
            }
            else {
                
                html += '<a onclick="window.plugin.nandn.naughtyandnice(' + a_level + ');">L' + a_level + '</a>&nbsp;&nbsp;';
            }
        }
        
        html += '</p>';
        
        if (show_table) {
            
            html += '<table class="nandn">';
            
            var our_teams = [ "RESISTANCE", "ALIENS" ];
            var our_lists = [ "PLAYERS", "PORTALS" ];
            
            for (var our_team = 0; our_team < our_teams.length; our_team++) {
                
                var rgba = (our_teams[our_team] == "ALIENS" ? '0, 255, 0' : '128, 128, 255');
                var desc = (our_teams[our_team] == "ALIENS" ? 'NICE' : 'NAUGHTY');
                
                for (var our_list = 0; our_list < our_lists.length; our_list++) {
                    
                    html += '<tr><th style="text-align: center; background-color: rgba(' + rgba + ', 0.25);" colspan="3">' + desc + " " + our_lists[our_list] + '</th></tr>';
                    
                    Object.keys(teams[our_teams[our_team]][our_lists[our_list]]).forEach(function (this_item) {
                        
                        var item_names = [];
                        
                        Object.keys(teams[our_teams[our_team]][our_lists[our_list]][this_item]).forEach(function (this_item_name) {
                            
                            item_names.push(this_item_name);
                            
                        });
                        
                        item_names.sort(function(a,b) { var A = a.toLowerCase(); var B = b.toLowerCase(); if (A < B) { return -1; } if (A > B) { return 1; } return 0; });
                        
                        html += '<tr><td style="color: rgba(' + rgba + ', 1);">';
                        
                        if (our_lists[our_list] == "PORTALS") {
                            
                            html += '<a title="Click to show details. Double click to show details and zoom" class="help" style="color: #00CC99;" ' +
                                    'onclick="window.renderPortalDetails(\'' + portal_guids[this_item].guid + '\');return false" ' + 
                                    'ondblclick="window.zoomToAndShowPortal(\'' + portal_guids[this_item].guid + '\', [' + (portal_guids[this_item].latE6 / 1000000) +',' + (portal_guids[this_item].lngE6 / 1000000) + ']);return false">' +
                                    this_item + '</a>';
                        }
                        else {
                            
                            html += this_item;
                        }
                        
                        html += '</td><td style="text-align: right";>' + item_names.length + '<td style="text-align: justify">' + item_names.join(" -- ") + '</td></tr>';
                    });
                }
            }
            
            html += '</table>';
        }
        
        alert('<div id="nandn">' + html + '</div>', true, function() {$(".ui-dialog").removeClass('ui-dialog-nandn');});
        $(".ui-dialog").addClass('ui-dialog-nandn');
    }
    
    var setup =  function() {
        
        $('#toolbox').append(' <a onclick="window.plugin.nandn.naughtyandnice(8);">Naughty&nbsp;&amp;&nbsp;Nice</a>');
        $('head').append('<style>.ui-dialog-nandn {position: absolute !important; top: 10px !important; left: 30px !important;max-width:800px !important; width:733px !important;}</style>');
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

