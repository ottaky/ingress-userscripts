// ==UserScript==
// @id             iitc-plugin-nice-details@ottaky
// @name           IITC plugin: Nice Details
// @version        1
// @description    ottaky's Nice Details
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.nicedetails = function() {};
    
    window.plugin.nicedetails.our_data = {};
    
    window.plugin.nicedetails.show = function() {
        
        var our_portal = window.plugin.nicedetails.our_data.portalDetails;
        
        if (our_portal && our_portal.portalV2) {
            
            console.log(our_portal);
            
            var our_alert = 'Portal Name: ' + our_portal.portalV2.descriptiveText.TITLE + '<br />' +
                            'Address: ' + our_portal.portalV2.descriptiveText.ADDRESS + '<br /><br />' +
                            'Intel: http://www.ingress.com/intel?latE6=' + our_portal.locationE6.latE6  + '&lngE6=' + our_portal.locationE6.lngE6 + '&z=17&pguid=' + window.selectedPortal + '<br /><br />' +
                            'GMaps: https://maps.google.co.uk/maps?q=' + (our_portal.locationE6.latE6 / 1000000) + ',' + (our_portal.locationE6.lngE6 / 1000000) + '&z=17<br /><br />' +
                            'Image: ' + our_portal.imageByUrl.imageUrl + '<br /><br />';
            
            var our_date = new Date(parseInt(our_portal.captured.capturedTime));
            
            our_alert += 'Team: ' + (our_portal.controllingTeam.team == "ALIENS" ? "Enlightened" : "Resistance") + '<br />' +
                         'Owner: ' + localStorage[our_portal.captured.capturingPlayerId] + '<br />' +
                         'Captured: ' + our_date + '<br />';
            
            var our_level = 0;
            var our_res = [];
            var our_potential_energy = 0;
            var our_energy = 0;
            
            for (var our_resonator = 0; our_resonator < our_portal.resonatorArray.resonators.length; our_resonator++) {
                
                if (our_portal.resonatorArray.resonators[our_resonator] && our_portal.resonatorArray.resonators[our_resonator].level) {
                    
                    our_level += our_portal.resonatorArray.resonators[our_resonator].level;
                    our_res.push(our_portal.resonatorArray.resonators[our_resonator].level);
                    our_energy += our_portal.resonatorArray.resonators[our_resonator].energyTotal;
                    our_potential_energy += window.RESO_NRG[our_portal.resonatorArray.resonators[our_resonator].level];
                }
                else {
                    
                    our_res.push(0);
                }
            }
            
            our_level = our_level / our_portal.resonatorArray.resonators.length;
            
            if (our_level < 1) {
                
                our_level = 1;
            }
            
            our_res.sort(function(a,b) { return b - a; });
            
            var our_res_string = our_res.join("");
            our_res_string = our_res_string.replace(/0/g, "-");
            
            our_alert += 'Level: ' + parseInt(our_level) + ' (' + our_level + ')<br />' +
                         'Energy: ' + our_energy + ' / ' + our_potential_energy + ' (' +  parseInt(((our_energy / our_potential_energy) * 100))  + '%)<br />' +
                         'Resonators: ' + our_res_string + '<br />';
            
            var our_shields = [];
            
            for (var our_shield = 0; our_shield < 4; our_shield++) {
                
                if (our_portal.portalV2.linkedModArray[our_shield] && our_portal.portalV2.linkedModArray[our_shield].rarity) {
                    
                    our_shields.push(our_portal.portalV2.linkedModArray[our_shield].rarity);
                }
            }        
            
            our_alert += 'Shields: ' + our_shields.join(", ") + '<br />' +
			             'Fields: ' + our_portal.portalV2.linkedFields.length + '<br />' +
                         'Links: ' + our_portal.portalV2.linkedEdges.length + '<br />';
            
            for (var our_link = 0; our_link < our_portal.portalV2.linkedEdges.length; our_link++) {
                
                our_alert += '&nbsp;&nbsp;&nbsp;Link ' + (our_portal.portalV2.linkedEdges[our_link].isOrigin ? 'to' : 'from') + ' ' + window.portals[our_portal.portalV2.linkedEdges[our_link].otherPortalGuid].options.details.portalV2.descriptiveText.TITLE + '<br />';
            }
            
            our_alert += '<br />ID: ' + window.selectedPortal;
            
            alert(our_alert);
        }
    }
    
    var setup =  function() {
        
        $('#toolbox').append(' <a onclick="window.plugin.nicedetails.show();">Nice&nbsp;Details</a>');
        window.addHook('portalDetailsUpdated', function(data) { window.plugin.nicedetails.our_data = data });
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

