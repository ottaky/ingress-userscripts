// ==UserScript==
// @name           IITC plugin: Meddler Mute Pro
// @version        1
// @description    Mute LondonMeddler in Faction Chat
// @include        http://www.ingress.com/intel*
// @include        https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    var setup = function() {
        window.addHook('factionChatDataAvailable', function(data) { 
        	Object.keys(data.processed).forEach(function(this_plext) {
                if (localStorage[data.processed[this_plext][3]] == "LondonMeddler") {
                 	delete data.processed[this_plext];
                }
            });
        });
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        setup();
    } else {
        if(window.bootPlugins)
            window.bootPlugins.push(setup);
        else
            window.bootPlugins = [setup];
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

