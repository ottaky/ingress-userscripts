SOME INGRESS USER SCRIPTS
=========================

These all require Tampermonkey or Greasemonkey.

INGRESS AUGMENT USER SCRIPT
===========================

ingress_augment.user.js
-----------------------

After I became bored with IITC regularly breaking I wrote this script as a lightweight replacement.

It provided a number of enhancements over the stock /intel map.

* if showing a location passed withing the URL, draw a circle around it.
* change some click events to mouseovers (to save clicking the same old stuff over and over)
* pre-fill the search box
* de-clutter the map by removing elements that nobody cares about
* increase the timeout for detecting human presence
* enhanced dates / times in COMM
* display the number of incoming and outgoing links on a portal
* show mod rarity
* show link destination names
* highlight specific players in COMM
* toggle field rendering
* export portal data in Garmin SatNav compatible format
* highlight player resonators and mods
* etc.

This all piggy-backed on the stock /intel code. Worked great until Niantic went back to minifying their JavaScript.

OLD IITC PLUGINS
================

I just found these IITC plugins that I wrote lurking on a hard drive. I've uploaded them for the sake of nostalgia.

AFAIK **none** of them will work any more. They were all written around 3 years ago and even then Niantic were fiddling with their code on a regular basis and breaking the plugin code with each change.

IITC looks to still be available here: [http://iitc.jonatkins.com/](http://iitc.jonatkins.com/)

new_jarvis.user.js
------------------

A chat bot (called JARVIS) that could receive and respond to messages in COMM. Was able to perform whois and whereis lookups for any Enlightened players on requested users. This was actually quite cool, but was a clear violation of the ToS so I only ever deployed it for fun a couple of times after development.

iitc_upgradeable.user.js
------------------------

This one found portals that you could upgrade and showed the potential new portal level.

iitc_nice_details.user.js
-------------------------

An enhanced portal details popup with some extra info displayed.

iitc_naughty_and_nice.user.js
-----------------------------

Summarised portal in a tabular form. I kind of local scoreboard.

iitc_muzzle_directive.user.js
-----------------------------

Highlighted Resistance portals >= L6 for targetting.

iitc_golden_oldies.user.js
--------------------------

Summarised the longest held portals. This was before Guardian badges, so it was of academic interest back then.

iitc_7up.user.js
----------------

Highlighted portals that you could upgrade to make them L8.

iitc_dimmer.user.js
-------------------

Reduced the map contrast to save wear and tear on the eyes.

iitc_meddler_mute_pro.user.js
-----------------------------

Discard all mesages from LondonMeddler

images
------

Some of the plugins overlayed images from my web server onto the intel map. These are the images. Any code that references http://ottaky.com/ingress/images was looking at these.



