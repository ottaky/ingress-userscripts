// ==UserScript==
// @id             iitc-plugin-golden-oldies@ottaky
// @name           IITC plugin: Golden Oldies
// @version        1
// @description    ottaky's Golden Oldies List
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.golden = function() {};
    
    window.plugin.golden.oldies = function() {   
        
        var our_portals = [];
        
        Object.keys(window.portals).forEach(function (this_portal) {
            
            if (window.portals[this_portal].options.details.captured) {
                
                our_portals.push( { "name"     : window.portals[this_portal].options.details.portalV2.descriptiveText.TITLE,
                                    "captured" : parseInt(window.portals[this_portal].options.details.captured.capturedTime),
                                    "owner"    : window.portals[this_portal].options.details.captured.capturingPlayerId,
                                    "team"     : window.portals[this_portal].options.details.controllingTeam.team,
                                    "guid"     : this_portal } );
            }
        });
        
        our_portals.sort(function(a,b) { return a.captured - b.captured; } );
        
        our_portals = our_portals.slice(0, 10);
        
        var html = '<table><tr><th>Date</th><th>Time</th><th>Team</th><th>Owner</th><th>Name</th></tr>';
        
        for (var our_portal = 0; our_portal < our_portals.length; our_portal++) {
            
            var our_date = new Date(our_portals[our_portal].captured);
            
            html += '<tr>' +
                    '<td>' + our_date.toLocaleDateString() + '</td>' + 
                    '<td>' + our_date.toLocaleTimeString() + '</td>' +
                    '<td>' + (our_portals[our_portal].team == "ALIENS" ? 'Enlightened' : 'Resistance') + '</td>' +
                    '<td>' + localStorage[our_portals[our_portal].owner] + '</td>' +
                    '<td><a title="Click to show details" class="help" style="color: #00CC99;" ' +
                    'onclick="window.renderPortalDetails(\'' + our_portals[our_portal].guid + '\');return false">' +
                    our_portals[our_portal].name +
                    '</a></td>' +
                    '</tr>';
        }
        
        html += '</table>';
        
        alert('<div id="golden">' + html + '</div>', true, function() {$(".ui-dialog").removeClass('ui-dialog-golden');});
        $(".ui-dialog").addClass('ui-dialog-golden');
    }
    
    var setup =  function() {
        
        $('#toolbox').append(' <a onclick="window.plugin.golden.oldies();">Golden&nbsp;Oldies</a>');
        $('head').append('<style>.ui-dialog-golden {position: absolute !important; top: 10px !important; left: 30px !important;max-width:800px !important; width:733px !important;}</style>');
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

