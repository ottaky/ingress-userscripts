// ==UserScript==
// @id             iitc-plugin-dimmer@ottaky
// @name           IITC plugin: Dimmer
// @version        1
// @description    ottaky's Dimmer
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// ==/UserScript==

function wrapper() {
    
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    window.plugin.dimmer = function() {};
    
    window.plugin.dimmer.value = 0;
    
    window.plugin.dimmer.dim = function() {
        
        if (document.getElementById("_GMapContainer")) {
            
            document.getElementById("_GMapContainer").style.backgroundColor = "#000000";
            document.getElementById("_GMapContainer").children[0].style.opacity = window.plugin.dimmer.value;
            
            window.plugin.dimmer.value += 0.25;
            
            if (window.plugin.dimmer.value > 1) {
                
                window.plugin.dimmer.value = 0;
            }
        }
    }
    
    var setup =  function() {
        
        $('#toolbox').append(' <a onclick="window.plugin.dimmer.dim();">Dimmer</a>');
    }
    
    if(window.iitcLoaded && typeof setup === 'function') {
        
        setup();
    }
    else {
        
        if(window.bootPlugins) {
            
            window.bootPlugins.push(setup);
        }
        else {
            
            window.bootPlugins = [setup];
        }
    }
}

var script = document.createElement('script');
script.appendChild(document.createTextNode('('+ wrapper +')();'));
(document.body || document.head || document.documentElement).appendChild(script);

